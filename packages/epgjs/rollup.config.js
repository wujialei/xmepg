import babel from "@rollup/plugin-babel"
import bubel from "@rollup/plugin-buble"
import commonjs from "@rollup/plugin-commonjs"
import nodeResolve from "@rollup/plugin-node-resolve"
import replace from "@rollup/plugin-replace"
import serve from 'rollup-plugin-serve'
import livereload from 'rollup-plugin-livereload'
import { uglify } from 'rollup-plugin-uglify'

let isDev = process.env.NODE_ENV == 'development'
let isStage = process.env.NODE_ENV == 'stage'
let config = [{
  input: "./src/index.js",
  output: {
    format: "umd",
    file: isDev ? "dist/epgjs.js" : "dist/epgjs.min.js",
    name: "epgjs",
    sourcemap: true,
    global: {
    },
  },
  ignoreGlobal: true,
  plugins: [
    replace({
      preventAssignment: true,
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_EN),
    }),
    nodeResolve({
      jsnext: true,
      main: true
    }),
    babel({
      exclude: "node_modules/**",
      babelHelpers: "bundled",
    }),
    commonjs(),
    bubel(),
    isDev && livereload({
      watch: ['dist', 'index.html', 'examples']
    }),
    isDev && serve({
      open: true,
      openPage: "index.html",
      port: 6002,
      contentBase: ""
    }),
    // 构建环境下才压缩代码
    !isDev && !isStage && uglify({
      compress: {
        drop_console: true
      }
    }),
  ]
}];
export default config;
