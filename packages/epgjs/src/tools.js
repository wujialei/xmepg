/**
 * 不依赖jQuery
 * 基础工具类
 */

/**
 * 是否是函数
 * @param {*} func 
 * @returns 
 */
 export const isFunction = function(func) {
  return typeof func === 'function'
}

/**
 * 是否是promise
 * @param {*} o 
 * @param {*} type 
 * @returns 
 */
export const isPromise = function(e) {
  return e && isFunction(e.then) && isFunction(e.catch);
}

/**
 * 事件promise函数,用于解决事件回调问题
 * @param {*} params 
 * @returns 
 */
export function eventFuncPromise(params) {
  return function (func) {
    return new Promise((reslove) => {
      let q = func(params, () => {
        reslove();
      })
      //支持promise行为
      if (q && q.then) {
        q.then((bool) => {
          bool && reslove();
        })
      }
      return q
    })
  }
}


/**
 * 创建一个promise实例, 取出resolve, reject
 * @return object {
 *   promise,
 *   resolve,
 *   reject
 * }
 */
 export const createPromise = function () {
  let _resolve = null
  let _reject = null
  const promise = new Promise((resolve, reject) => {
    _resolve = resolve
    _reject = reject
  })
  return {
    promise,
    resolve: _resolve,
    reject: _reject
  }
}

/**
 * 创建不可重复id
 * @returns 
 */
export const getUniqueId = () => {
  return (Math.random() + Date.now() + Math.random()).toString(36).replace(/\./, "")
}

/**
 * 是否是html元素
 */
 export function isHTMLElement(el) {
  return el instanceof HTMLElement;
}

/**
 * 检测对象是否含有对应的属性
 * @param {Object} obj 查询对象
 * @param {String} attr 查询属性
 * @returns Boolean 判断对象是否含有该属性
 */
export const hasOwn = (obj, attr) => Object.prototype.hasOwnProperty.call(obj, attr);

/**
 * 尝试元素转换为JSON对象
 * @param {*} value 
 * @returns 
 */
export function string2json(value) {
  try {
    return JSON.parse(value)
  } catch (e) {
    return value;
  }
}

/**
 * 尝试将数组转为JSON字符串
 * @param {*} obj 
 * @returns 
 */
export function json2string(obj) {
  if (typeof obj === 'object' && obj !== null) {
    return JSON.stringify(obj)
  }
  return obj;
}

/**
 * 获取元素dataset兼容处理
 * @param {DOM} el 元素节点
 * @param {String} name 属性名称
 */
export const getDataSet = (el, name) => {
  let dataset = {};
  let rep = new RegExp("-([a-z])", "gi");
  let nameStr =
    typeof name === "string"
      ? name.replace(rep, (all, letter) => letter.toUpperCase())
      : "";
  if (el.dataset) {
    if (nameStr) {
      return string2json(el.dataset[nameStr]);
    } else {
      Object.keys(el.dataset).forEach((k) => {
        dataset[k] = string2json(el.dataset[k])
      })
      return dataset;
    }
  } else {
    let attrs = el.attributes,
      reg = new RegExp("^data-(.+)"),
      repReg = new RegExp("-([a-z])", "gi");
    Array.from(attrs).forEach((value) => {
      if (!value.specified) return;
      let matchStr = value.name.match(reg);
      if (matchStr) {
        name = matchStr[1].replace(repReg, (all, letter) =>
          letter.toUpperCase()
        );
        dataset[name] = string2json(value.value);
      }
    });
    return nameStr ? dataset[nameStr] : dataset;
  }
};

/**
 * 设置dataset属性兼容方法
 * @param {DOM} el 元素节点
 * @param {String/Object} name 属性名称/需要设置的属性的对象集合
 * @param {String} value 属性值
 */
export const setDataSet = (el, name, value) => {
  if (typeof name === "string") {
    if (el.dataset) {
      el.dataset[name] = json2string(value);
    } else {
      let reg = new RegExp("([A-Z])", "g");
      let attr =
        "data-" +
        name.replace(reg, (match, letter) => "-" + letter.toLowerCase());
      el.attributes[attr] = json2string(value);
    }
  } else if (typeof name === "object" && name !== null) {
    for(let k in name) {
      setDataSet(el, k, name[k]);
    }
  }
};

/**
 * 获取元素属性 存在略微性能问题可以忽略
 * @param {*} elem 
 * @param {*} name  属性名
 * @returns 
 */
export const getAttr = function (elem, name) {
  if(name) {
    let value = elem.getAttribute(name);
    if(value === null) return undefined;
    return string2json(value);
  } 
  let attrs = {};
  Array.from(elem.attributes).forEach(attr => {
    if (!attr.specified) return;
    let tname = attr.name;
    attrs[tname] = string2json(attr.value);
  })
  return attrs;
}

/**
 * 设置属性
 * @param {*} elem 
 * @param {Object/String} name  属性名
 * @param {*} value 属性值
 * @returns 
 */
export const setAttr = function (elem, name, value) {
  if(typeof name =='string') {
    elem.setAttribute(name, json2string(value))
  } else if(typeof name === 'object' && name !== null) {
    for(let k in name) {
      elem.setAttribute(k, json2string(name[k]))
    }
  }
}

/**
 * 获取或设置属性
 * @param {*} elem html元素
 * @param {*} name 
 * @param {*} value 
 * @returns 
 */
export const attr = function(elem, name, value) {
  if(typeof name === 'object' && name !== null) {
    //写入属性
    for(let k in name) {
      elem.setAttribute(k, json2string(name[k]))
    }
  } else if(typeof name === 'string' && name && value !== undefined) {
    elem.setAttribute(name, json2string(value))
  } else if(typeof name === 'string' && name) {
    //获取单个属性
    value = elem.getAttribute(name);
    if(value === null) return undefined;
    return string2json(value);
  } else {
    //获取全部属性
    let attrs = {};
    Array.from(elem.attributes).forEach(attr => {
      if (!attr.specified) return;
      let tname = attr.name;
      attrs[tname] = string2json(attr.value);
    })
    return attrs;
  }
}

/**
 * 所有关于元素选择的工具函数均在Utils中
 */
/**
 * 计算两个点间的距离
 * @param {*} cx 
 * @param {*} cy 
 * @param {*} nx 
 * @param {*} ny 
 * @returns 
 */
 function distance(cx, cy, nx, ny) {
  let d = parseInt(Math.sqrt(Math.pow(cx - nx, 2) + Math.pow(cy - ny, 2)));
  return d;
}

/**
 * 判断n元素在p元素指定方向上的交集两个元素的距离支持交集计算
 * @param {String} direction //指定方向 left, right, up, down
 * @param {Number} pl 点1信息
 * @param {Number} pt
 * @param {Number} pw 
 * @param {Number} ph 
 * @param {Number} nl 点2信息
 * @param {Number} nt 
 * @param {Number} nw 
 * @param {Number} nh 
 * @param {Number} ratio //最大支持的交集比例 默认0.33
 */
function overlapDistance(direction, pl, pt, pw, ph, nl, nt, nw, nh, ratio = 0.33) {
  //判断x轴方向是否有重叠, 
  let pr = pl + pw; //p点的右侧x坐标
  let pb = pt + ph; //p点的下方y坐标
  let nr = nl + nw; //n点的右侧x坐标
  let nb = nt + nh; //n点的下方y坐标
  let ds = 99999999;
  if(direction == 'left') {
    //元素n是否在p的左侧，最大交集比例ratio
    if(nr <= pl) {
      //元素n直接在p左侧
      ds = Math.min.apply(null, [
        //当前左上 到 目标右上
        distance(pl, pt, nr, nt),
        //当前左上 到 目标右下
        distance(pl, pt, nr, nb),
        //当前左下 到 目标右下
        distance(pl, pb, nr, nb),
        //当前左下 到 目标右上
        distance(pl, pb, nr, nt),
        //中心点距离
        distance(pl, pt + ph * 0.5, nr, nt + nh * 0.5),
      ]);
      //查询水平距离，n元素在y轴方向上完全包裹p元素
      if(nt <= pt && pb <= nb) {
        ds = Math.min.apply(null, [ds, pl - nr]);
      }
    } else if(nr <= pl + pw * ratio && (
      (pt <= nt && nt <= pb) || 
      (pt <= nb && nb <= pb) ||
      (nt <= pt && pt <= nb) || 
      (nt <= pb && pb <= nb)
    )) {
      //元素n与p有交集
      ds = 0;
    }
  } else if(direction == 'right') {
     //元素n是否在p的右侧，最大交集比例ratio
     if(pr <= nl) {
      //元素n直接在p左侧
      ds = Math.min.apply(null, [
        //当前右上 到 目标左上
        distance(pr, pt, nl, nt),
        //当前右上 到 目标左下
        distance(pr, pt, nl, nb),
        //当前右下 到 目标左下
        distance(pr, pb, nl, nb),
        //当前右下 到 目标左上
        distance(pr, pb, nr, nt),
        //中心点距离
        distance(pr, pt + ph * 0.5, nl, nt + nh * 0.5),
      ]);
      //查询水平距离，n元素在y轴方向上完全包裹p元素
      if(nt <= pt && pb <= nb) {
        ds = Math.min.apply(null, [ds, nl - pr]);
      }
    } else if(pr - ratio * pw <= nl && (
      (pt <= nt && nt <= pb) || 
      (pt <= nb && nb <= pb) ||
      (nt <= pt && pt <= nb) || 
      (nt <= pb && pb <= nb)
    )) {
      ds = 0;
    }
  } else if(direction == 'up') {
    //元素n是否在p的上方，最大交集比例ratio
    if(nb <= pt) {
      //元素n直接在p上方
      ds = Math.min.apply(null, [
        //当前左上 到 目标左下
        distance(pl, pt, nl, nb),
        //当前左上 到 目标右下
        distance(pl, pt, nr, nb),
        //当前右上 到 目标左下
        distance(pr, pt, nl, nb),
        //当前右上 到 目标左上
        distance(pr, pt, nr, nb),
        //中心点距离
        distance(pl + pw * 0.5, pt, nl + nw * 0.5, nb),
      ]);
      //查询垂直距离，n元素在x轴方向上完全包裹p元素
      if(nl <= pl && pr <= nr) {
        ds = Math.min.apply(null, [ds, pt - nb])
      }
    } else if(nb <= pt + ratio * ph && (
      (pl <= nl && nl <= pr) || 
      (pl <= nr && nr <= pr) || 
      (nl <= pl && pl <= nr) || 
      (nl <= pr && pr <= nr)
    )) {
      ds = 0;
    }
  } else if(direction == 'down') {
    //元素n是否在p的上方，最大交集比例ratio
    if(pb <= nt) {
      //元素n直接在p上方
      ds = Math.min.apply(null, [
        //当前左下 到 目标左上
        distance(pl, pb, nl, nt),
        //当前左下 到 目标右上
        distance(pl, pb, nr, nt),
        //当前右下 到 目标左上
        distance(pr, pb, nl, nt),
        //当前右下 到 目标右上
        distance(pr, pb, nr, nt),
        //中心点距离
        distance(pl + pw * 0.5, pb, nl + nw * 0.5, nt),
      ]);
      //查询垂直距离，n元素在x轴方向上完全包裹p元素
      if(nl <= pl && pr <= nr) {
        ds = Math.min.apply(null, [ds, nt - pb]);
      }
    } else if(pb - ratio * ph <= nt && (
      (pl <= nl && nl <= pr) || 
      (pl <= nr && nr <= pr) || 
      (nl <= pl && pl <= nr) || 
      (nl <= pr && pr <= nr)
    )) {
      ds = 0;
    }
  }
  return ds;
}

/**
 * 用于元素是否符合条件
 * 可基于重叠判断，但按照规则，重叠部分不要大于30%否则过滤
 * @param {*} direction 
 * @param {*} ci  //当前焦点的定位信息
 * @param {*} ei //需要对比元素的定位信息
 * @param {*} el //需要对比元素的Creater对象
 * @param {*} nextObj //存储最终对比的元素
 * {
 *   pointer: pointer, //焦点元素
 *   next: null, //下个元素
 *   distance: 99999999, //最短距离
 * }
 */
 export function checkNext(direction, ci, ei, el, nextObj) {
  //检查对比元素是否不可见
  if(ei.w === 0 || ei.h === 0) return nextObj;
  //获取不同方向上的距离
  let ds = overlapDistance(direction, ci.l, ci.t, ci.w, ci.h, ei.l, ei.t, ei.w, ei.h)
  if (ds < nextObj.distance && ds < nextObj.maxDistance) {
    nextObj.next = el;
    nextObj.distance = ds;
  }
  return nextObj;
}

/**
 * 获取元素e在parent中画布的位置，当parent = null是， 默认为parent 默认为body/html元素
 * @param {*} e 
 * @param {*} parent 
 * @returns 
 */
 export function getPosition(e, parent = null) {
  let l = 0, t = 0, w = e.offsetWidth, h = e.offsetHeight;
  while (e != null && (parent == null || parent !== e)) {
    l += e.offsetLeft;
    t += e.offsetTop;
    e = e.offsetParent;
  }
  return {
    l: l,
    t: t,
    w: w,
    h: h,
  };
}