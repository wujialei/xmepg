import Q from './Q'
import { getPosition, getUniqueId, attr, isHTMLElement, setDataSet, getDataSet } from './tools'
import animate from './animate';


//获取scrollLeft
function scrollLeft(ele) {
  var element = ele || document.body;
  return element.scrollLeft || (document.documentElement && document.documentElement.scrollLeft);
}
//获取scrollTop
function scrollTop(ele) {
  var element = ele || document.body;
  return element.scrollTop || (document.documentElement && document.documentElement.scrollTop);
}
/**
 * todo 需要移除jquery依赖， 暂时只依赖jqueryAnimate
 * 描述滚轴对象
 * 用于滚轴行为处理
 * <div class="epg-scroll" scroll-container>
 *  <div class="" scroll-document></div>
 * </div>
 * 滚动描述
 * 容器属性：
 *  scroll-container //垂直滚轴
 *  scroll-container="horiz" //水平滚轴
 *  scroll-absolute //指定使用绝对定位来完成滚动, 否则为scroll滚动
 *  scroll-animate-time //滚动时长，默认为500ms
 * 文档属性
 *  scroll-document //指定滚轴的滚动文档
 * 
 */
function Scroller(el) {
  //获得容器对象
  let box = isHTMLElement(el) ? el : null;
  let scrollContainer = attr(box, 'scroll-container');
  //必须存在scroll属性
  let isScrollBox = scrollContainer !== undefined;
  if (!box || !isScrollBox) {
    return {};
  }
  let doc;
  //查找指定的元素
  Array.from(box.children).forEach(elc => {
    if (attr(elc, 'scroll-document') !== undefined && !doc) {
      doc = elc;
    }
  })
  if (!doc) {
    return {};
  }
  //滚轴id
  this.uid = getUniqueId();
  attr(box, 'uid', this.uid);
  //滚动类型，是否为滚轴滚动，否则为绝对定位
  this.isScroll = attr(box, "scroll-absolute") === undefined;
  //滚动方向
  this.isHoriz = scrollContainer == 'horiz';
  //滚动最大时长
  this.animateTime = parseInt(attr(box, "scroll-animate-time"), 10) || 100;
  //容器
  this.$el = this.box = box;
  //文档
  this.doc = doc;
  //容器宽度
  this.bw = 0;
  //容器高度
  this.bh = 0;
  //文档宽度
  this.dw = 0;
  //文档高度
  this.dh = 0;
  //滚轴上下卷曲
  this.st = 0;
  //滚轴左右卷曲
  this.sl = 0;
  //设置父滚轴对象
  this.parentScroller = null;
}

Scroller._caches = {};
Scroller.create = function (elem) {
  if (!elem) return null;
  if (elem && elem instanceof Scroller) {
    return elem;
  }
  let uid = getDataSet(elem, 'uid');
  let obj = Scroller._caches[uid];
  if (obj && obj instanceof Scroller) {
    return obj;
  }
  obj = new Scroller(elem);
  if (!obj.uid) return null;
  Scroller._caches[obj.uid] = obj;
  return obj
}

/**
 * 用于回收资源
 * @param {*} creaters 
 */
Scroller.recycle = function (scrollers) {
  //情况用于缓存优化
  scrollers.forEach(obj => {
    delete Scroller._caches[obj.uid]
  })
}

Scroller.prototype = {
  constructor: Scroller,
  /**
   * 
   * 获取元素在画布上的位置信息
   * @param {*} creater 操作元素
   * @param {*} root 画布element
   * @returns 
   */
  getPosition(creater, root) {
    let parent = this;
    let pos = Object.assign({}, creater.pos);
    do {
      let st = parent.st; //parent.scrollTop();
      let sl = parent.sl; //parent.scrollLeft();
      if (parent === this && creater.scrollPos) {
        let sPos = creater.scrollPos;
        //计算在当前轴内是否超界导致不可见
        let l = sPos.l - sl;
        let r = l + sPos.w;
        let t = sPos.t - st;
        let b = t + sPos.h;
        if (r <= 0 || l >= this.bw || t >= this.bh || b <= 0) {
          pos.w = 0;
          pos.h = 0;
        }
      }
      pos.t = pos.t - st;
      pos.l = pos.l - sl;
      if (parent.$el === root) {
        parent = null;
      } else {
        parent = parent.parentScroller;
      }
    } while (parent);
    return pos;
  },
  /**
   * 设置父滚轴
   * @param {*} scroller 
   * @returns 
   */
  setParentScroller(scroller) {
    if (scroller instanceof Scroller) {
      this.parentScroller = scroller;
    }
    return this;
  },

  /**
   * 判断是否是孩子
   */
  isChild(elem) {
    let parent = elem.parentNode;
    let body = Q('body').get(0);
    let bool = false;
    while (1) {
      if (parent === this.box) {
        bool = true;
        break;
      }
      if (parent === body) break;
      parent = parent.parentNode;
    }
    return bool;
  },

  /**
   * 获取父轴的id
   * @returns 
   */
  getParentScrollerId() {
    let parentScrollerId = null;
    let parent = this.$el.parentNode;
    let body = Q('body').get(0);
    while (1) {
      if (parent === body) {
        break;
      }
      let uid = attr(parent, 'uid');
      if (uid && attr(parent, 'epg-scroll') !== undefined) {
        parentScrollerId = uid;
        break;
      } else {
        parent = parent.parentNode;
      }
    }
    return parentScrollerId;
  },
  /**
   * y轴位置获取
   * @returns 
   */
  scrollTop() {
    if (this.isScroll) {
      return scrollTop(this.$el);
    } else {
      let px = this.doc.style.top || "0px";
      let match = px.match(/^((\-)?\d+)px$/);
      if (match) {
        px = parseInt(match[1], 10);
        return px === 0 ? 0 : -1 * px;
      }
      return 0;
    }
  },
  /**
   * x轴位置获取
   * @returns 
   */
  scrollLeft() {
    if (this.isScroll) {
      return scrollLeft(this.$el);
    } else {
      let px = this.doc.style.left || "0px";
      let match = px.match(/^((\-)?\d+)px$/);
      if (match) {
        px = parseInt(match[1], 10);
        return px === 0 ? 0 : -1 * px;
      }
      return 0;
    }
  },
  /**
   * 更新滚轴信息
   */
  update() {
    let box = this.box;
    let doc = this.doc;
    this.bw = box.clientWidth;
    this.bh = box.clientHeight;
    this.dw = doc.clientWidth;
    this.dh = doc.clientHeight;
    this.st = this.scrollTop();
    this.sl = this.scrollLeft();
    return this;
  },
  /**
   * 滚动到起点
   */
  scrollStart() {
  },
  /**
   * 滚动到尾部
   */
  scrollEnd() {

  },
  /**
   * 动画依然依赖jquery
   * 触发滚动
   */
  _scroll(px, time) {
    //计算有效的滚动时间
    return new Promise((resolve) => {
      let params = {};
      let dom = null;
      if (this.isScroll) {
        if (this.isHoriz) {
          params.scrollLeft = px + 'px';
        } else {
          params.scrollTop = px + 'px';
        }
        dom = Q(this.box).get(0)
      } else {
        if (this.isHoriz) {
          px = px + this.sl;
          params.left = (-1 * px) + 'px';
        } else {
          px = px + this.st;
          params.top = (-1 * px) + 'px';
        }
        dom = Q(this.doc).get(0)
      }
      animate(dom, params, time, () => {
        this.update();
        resolve()
      })
    })
  },
  /**
   * 触发滚动到指定px位置
   */
  scroll(px, time) {
    //处理无需滚动时间逻辑
    time = time === 0 ? 0 : parseInt(time, 10) || this.animateTime;
    //开始计算最大可滚动范围
    // let max = 0;
    // if (this.isHoriz) {
    //   //水平方向上
    //   max = this.dw - this.bw;
    // } else {
    //   max = this.dh - this.bh;
    // }
    // if (px >= max) {
    //   px = max;
    // }
    return this._scroll(px, time);
  },

  /**
   * 触发滚动到指定的内部元素
   * @param {*} el 
   */
  scrollTo(el, time) {
    // return Promise.resolve();
    let box = this.box;
    if (!isHTMLElement(el)) return Promise.resolve();
    //获取滚动元素在滚轴内的位置
    let pos = getPosition(el, box);
    let gap = 0;
    let alignLimit = 1;
    let align = 'center'
    let swh, dwh, slt, nlt, nrb, nwh;
    if (this.isHoriz) {
      swh = this.bw;
      dwh = this.dw;
      slt = this.sl;
      nlt = pos.l;
      nrb = pos.l + pos.w;
      nwh = pos.w;
    } else {
      swh = this.bh;
      dwh = this.dh;
      slt = this.st;
      nlt = pos.t;
      nrb = pos.t + pos.h;
      nwh = pos.h;
    }
    //判断元素是否已经完全展示

    let featureStop = 0, stop = 0;
    if (align === 'top') { //主轴方向顶部
      featureStop = nlt - gap;
    } else if (align === 'bottom') { //主轴方向尾部
      featureStop = nrb - swh + gap;
    } else { //主轴方向居中
      featureStop = nlt + nwh / 2 - swh / 2;
    }
    //期望滚动不能超过滚轴长度
    // if(dwh > 0) {
    //   let maxStop = dwh - swh >= 0 ? dwh - swh : 0;
    //   featureStop > maxStop && (featureStop = maxStop);
    // }
    //判断元素是否完成展示 
    // if (nlt > slt && nrb < slt + swh && alignLimit === 1) {
    //   if (nlt < swh && nrb < swh) {
    //     stop = 0;
    //   } else {
    //     stop = slt;
    //   }
    // } else {
    //   if (nlt < swh * alignLimit && nrb < swh * alignLimit) {
    //     stop = 0;
    //   } else {
    //     stop = featureStop >= 0 ? Math.floor(featureStop) : 0;
    //   }
    // }
    return this.scroll(featureStop, time)
    // return this.scroll(this.isHoriz ? pos.l : pos.t, time)
  }

}


export default Scroller;