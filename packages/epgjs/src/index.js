import './browser.polyfill';
import {bootstrapCss} from './index.css'
import Service from './service'
import Q from './Q'
window.Q = Q;
// "vuepress": "^1.2.0",
// "vuepress-plugin-container": "^2.1.2",
// "vuepress-plugin-demo-code": "^0.5.0",
// "vuepress-plugin-live": "^1.4.2"

// export { default as LPopup } from './components/LPopup';
export default function epgjs(options) {
  if(options.loadCss !== false) {
    bootstrapCss();
  }
  return new Service(options)
}

epgjs.$ = Q;

epgjs.EGPJS = Service;