/**
 * 不依赖jQuery
 * 焦点元素
 */
import { getDataSet, setDataSet, isHTMLElement, getUniqueId, attr } from './tools'
import Grouper from './grouper'
import Scroller from './scroller'
/**
 * 创建可移动元素
 * @param {*} elem html_ELEMENT元素
 */
function Creater(elem) {
  if (!isHTMLElement(elem)) return {};
  //存储dom节点
  this.$el = elem;
  //存储节点样式 原本的class
  this.classname = '';
  //基础属性
  this.attributes = null;
  //data属性
  this.dataset = {}
  //创建元素唯一id
  this.uid = getUniqueId();
  this.setDataset('uid', this.uid);
  //获取获得焦点样式
  this.focusClass = null;
  //所属的grouper
  this.grouper = null;
  //归属的scroll
  this.scroller = null;
  //pos信息
  this.pos = null;
  //scorllPos 在scroll中的定位信息
  this.scrollPos = null;
  //手动更新
  this.update();
}

Creater.prototype = {
  constructor: Creater,
  /**
   * 设置所在组的id
   */
  setGouper(grouper) {
    if (grouper instanceof Grouper) {
      this.grouper = grouper;
    }
    return this;
  },

  setScroller(scroller) {
    if (scroller instanceof Scroller) {
      this.scroller = scroller;
    }
    return this;
  },

  /**
   * 元素被更新时触发
   */
  update() {
    this.attributes = attr(this.$el);
    this.focusClass = this.attributes.focusclass || 'focus';
    this.classname = this.attributes.class || "";
    this.dataset = this._dataset();
    this.pos = null;
    return this;
  },
  //设置元素pos信息
  setPos(pos) {
    //定位信息
    this.pos = pos || null;
  },
  //设置其父节点scroll中的pos信息
  setScrollPos(pos) {
    this.scrollPos = pos;
  },
  /**
   * 获取属性
   */
  _dataset() {
    return getDataSet(this.$el)
  },

  /**
   * 设置属性
   */
  setDataset(name, value) {
    setDataSet(this.$el, name, value)
    return this;
  },

  /**
   * 移除焦点样式
   */
  removeFocusClass() {
    this.removeClass(this.focusClass)
    return this;
  },

  /**
   * 追加焦点样式
   * @param {*} classname 
   */
  addFocusClass() {
    this.addClass(this.focusClass)
    return this;
  },

  /**
   * addClass
   */
  addClass(classname) {
    let cls = this.classname.split(' ');
    let inCls = classname.split(' ');
    inCls.forEach(v => {
      if (cls.indexOf(v) < 0) {
        cls.push(v)
      }
    })
    this.classname = cls.join(" ");
    //执行修改classs属性
    attr(this.$el, 'class', this.classname)
    return this;
  },

  /**
   * removeClass
   */
  removeClass(classname) {
    let cls = this.classname.split(' ');
    let delCls = classname.split(' ');
    let inCls = [];
    cls.forEach(v => {
      if (delCls.indexOf(v) < 0) {
        inCls.push(v)
      }
    });
    //执行修改classs属性
    this.classname = inCls.join(" ");
    attr(this.$el, 'class', this.classname)
    return this;
  },

  /**
   * hasClass
   * @param {*} classname 
   */
  hasClass(classname) {
    let cls = this.classname.split(' ');
    let checkCls = classname.split(' ');
    //判断样式是否存在
    let bool = true;
    checkCls.forEach(v => {
      if (cls.indexOf(v) < 0) {
        bool = false;
      }
    })
    return bool;
  },
}

Creater._caches = {};
Creater.create = function (elem, options) {
  if (!elem) return null;
  if (elem && elem instanceof Creater) {
    return elem;
  }
  let uid = getDataSet(elem, 'uid');
  let obj = Creater._caches[uid];
  if (obj && obj instanceof Creater) {
    return obj;
  }
  obj = new Creater(elem, options);
  if (!obj.uid) return null;
  Creater._caches[obj.uid] = obj;
  return obj
}


/**
 * 用于回收资源
 * @param {*} creaters 
 */
Creater.recycle = function (creaters) {
  //情况用于缓存优化
  creaters.forEach(creater => {
    delete Creater._caches[creater.uid]
  })
}



export default Creater;