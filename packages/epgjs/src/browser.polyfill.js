/**
 * 用于兼容代码中使用的低版本代码不兼容
 */

/**
 * 兼容Function.bind
 */
if (!Function.prototype.bind) {
  Function.prototype.bind = function (oThis) {
    if (typeof this !== 'function') {
      throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');
    }
    var aArgs = Array.prototype.slice.call(arguments, 1),
      fToBind = this,
      fNOP = function () { },
      fBound = function () {
        return fToBind.apply(this instanceof fNOP
          ? this
          : oThis,
          // 获取调用时(fBound)的传参.bind 返回的函数入参往往是这么传递的
          aArgs.concat(Array.prototype.slice.call(arguments)));
      };

    // 维护原型关系
    if (this.prototype) {
      // Function.prototype doesn't have a prototype property
      fNOP.prototype = this.prototype;
    }
    fBound.prototype = new fNOP();

    return fBound;
  };
}
/**
 * 兼容trim
 */
if (!String.prototype.trim) {
  String.prototype.trim = function () {
    return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
  };
}

/**
 * 兼容Array.from
 */
if (!Array.from) {
  //将类似的数组数据转为数组
  Array.from = function (arr) {
    return Array.prototype.slice.apply(arr);
  }
}

/**
 * 兼容Object.assign
 */
if (typeof Object.assign != 'function') {
  Object.assign = function (target) {
    if (target === undefined || target === null) {
      throw new TypeError('Cannot convert undefined or null to object');
    }
    var output = Object(target);
    for (var index = 1; index < arguments.length; index++) {
      var source = arguments[index];
      if (source !== undefined && source !== null) {
        for (var nextKey in source) {
          if (source.hasOwnProperty(nextKey)) {
            output[nextKey] = source[nextKey];
          }
        }
      }
    }
    return output;
  };
}

/**
 * 兼容ArrayMap函数
 * https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Array/map
 */
function ArrayMap(callback) {
  var T, A, k;
  if (this == null) {
    throw new TypeError('this is null or not defined');
  }
  var O = Object(this);
  var len = O.length >>> 0;
  if (typeof callback !== 'function') {
    throw new TypeError(callback + ' is not a function');
  }
  if (arguments.length > 1) {
    T = arguments[1];
  }
  A = new Array(len);
  k = 0;
  while (k < len) {
    var kValue, mappedValue;
    if (k in O) {
      kValue = O[k];
      mappedValue = callback.call(T, kValue, k, O);
      A[k] = mappedValue;
    }
    k++;
  }
  return A;
};

/**
 * 兼容Array.prototype.map
 */
if (!Array.prototype.map) {
  Array.prototype.map = ArrayMap;
}

/**
 * 兼容Array.prototype.forEach
 */
if (!Array.prototype.forEach) {
  Array.prototype.forEach = ArrayMap;
}

/**
 * 兼容Array.prototype.includes
 */
if (!Array.prototype.includes) {
  Array.prototype.includes = function (search, fromIndex) {
    var index = this.indexOf(search);
    fromIndex = parseInt(fromIndex, 10) || 0;
    return index >= fromIndex;
  }
}

/**
 * 兼容Object.keys
 */
if (!Object.keys) {
  Object.keys = (function () {
    'use strict';
    var hasOwnProperty = Object.prototype.hasOwnProperty,
      hasDontEnumBug = !({ toString: null }).propertyIsEnumerable('toString'),
      dontEnums = [
        'toString',
        'toLocaleString',
        'valueOf',
        'hasOwnProperty',
        'isPrototypeOf',
        'propertyIsEnumerable',
        'constructor'
      ],
      dontEnumsLength = dontEnums.length;

    return function (obj) {
      if (typeof obj !== 'function' && (typeof obj !== 'object' || obj === null)) {
        throw new TypeError('Object.keys called on non-object');
      }

      var result = [], prop, i;

      for (prop in obj) {
        if (hasOwnProperty.call(obj, prop)) {
          result.push(prop);
        }
      }

      if (hasDontEnumBug) {
        for (i = 0; i < dontEnumsLength; i++) {
          if (hasOwnProperty.call(obj, dontEnums[i])) {
            result.push(dontEnums[i]);
          }
        }
      }
      return result;
    };
  }());
}

/**
 * 兼容Promise.resolve
 */
if (Promise && !Promise.resolve) {
  Promise.resolve = function (data) {
    return new Promise(function (resolve) {
      resolve(data)
    })
  }
}

/**
 * 兼容Promise.reject
 */
if (Promise && !Promise.reject) {
  Promise.reject = function (err) {
    return new Promise(function (resolve, reject) {
      reject(err);
    })
  }
}


