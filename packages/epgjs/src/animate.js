/**
 * 动画函数,该动画函数主要是解决滚轴唯一问题
 */

let requestAFrame = function () {
  return window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    function (func) {
      return window.setTimeout(func, 1E3 / 60)
    };
}();
let cancelAFrame = function() {
  return window.cancelAnimationFrame || 
    window.webkitCancelAnimationFrame ||
    window.mozCancelAnimationFrame ||
    window.oCancelAnimationFrame ||
    function(handle) {
      window.clearTimeout(handle)
    }
}();


function getPxVal(px) {
  px = (px + "").trim();
  let match = px.match(/^(\-?\d+)/)
  if(match && match[1]) {
    return parseInt(match[1], 10) || 0
  }
  return 0;
}

function isScrollTL(key) {
  return key == 'scrollTop' || key == 'scrollLeft';
}

function isPositionTL(key) {
  return key == 'top' || key == 'left';
}

function inAni(key) {
  return ['top', 'left', 'scrollTop', 'scrollLeft'].indexOf(key) >= 0;
}

export default function animate(elm, params, duration, callback) {
  let t = + new Date();
  let a = null;
  duration = duration > 0 ? duration : 1;
  let oprs = {};
  Object.keys(params || {}).forEach(type => {
    if(!inAni(type)) return;
    let start = 0, end = getPxVal(params[type]);
    if(isPositionTL(type)) {
      start = getPxVal(elm.style[type])
    } else if(isScrollTL(type)) {
      start = getPxVal(elm[type]);
    }
    oprs[type] = {
      start: start,
      end: end,
    }
  })
  function run() {
    let nt = + new Date();
    let s = 1 - (Math.max(0, t + duration - nt) / duration || 0);
    let ani = function() {
      Object.keys(oprs).forEach(type => {
        let opr = oprs[type];
        let n = (opr.end - opr.start) * s + opr.start
        if(isScrollTL(type)) {
          elm[type] = n;
        } else if(isPositionTL(type)) {
          elm.style[type] = n + 'px';
        }
      })
    }
    ani();
    if(s === 1) {
      a && cancelAFrame(a)
      typeof callback === 'function' ? callback() : '';
    } else {
      a = requestAFrame(run)
    }
  }
  a = requestAFrame(run)
}
