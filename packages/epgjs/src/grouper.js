import Q from './Q'
import Creater from './creater'
import { getAttr, setAttr, getUniqueId, isHTMLElement } from './tools'
const EPG_GROUP = 'group-container'
/**
 * 元素组，关于组的操作逻辑
 */
function Grouper(elem, options = {}) {
  if (!isHTMLElement(elem)) return {};
  this.attributes = getAttr(elem);
  let uid = this.attributes['uid'];
  if (!uid) {
    uid = getUniqueId();
    setAttr(elem, 'uid', uid);
    this.attributes['uid'] = uid;
  }
  this.$el = elem;
  this.selector = options.selector || '';
  this.selectedClass = this.attributes.selectedclass || options.selectedClass || 'epg-actived';
  this.uid = uid;
  //存放焦点元素
  this.items = [];
  //是否自动选中, 默认非自动选中
  this.isAuto = this.attributes['group-selected-auto'] !== false;
  //扫描组内元素
  this.update();
}

Grouper.prototype = {
  constructor: Grouper,
  update() {
    this.items = [];
    Q(this.$el).find(this.selector).toArray().forEach((el) => {
      this.addItem(Creater.create(el).setGouper(this))
    })
  },

  /**
   * 获得组内的选中元素
   */
  getSelected() {
    let selected = [];
    for (let i = 0; i < this.items.length; i++) {
      let creater = this.items[i];
      if (creater.hasClass(this.selectedClass)) {
        selected.push(creater)
      }
    }
    return selected.length > 0 ? selected[0] : this.items[0]
  },

  selected(nexter, pointer) {
    if (!this.isAuto) return;
    //设置选中
    if (this.items.indexOf(pointer) >= 0) {
      //同轴元素
      pointer.removeClass(this.selectedClass)
    }
    //下一个元素添加
    if (this.items.indexOf(nexter) >= 0) {
      nexter.addClass(this.selectedClass)
    }
  },

  /**
   * 添加改区域下所有可操作元素
   */
  addItem(creater) {
    if (this.items.indexOf(creater) < 0) {
      this.items.push(creater);
    }
  },
}

Grouper._caches = {};
Grouper.create = function (elem, options) {
  let uid = getAttr(elem, 'uid');
  let grouper = Grouper._caches[uid];
  if (grouper && grouper instanceof Grouper) {
    return grouper;
  }
  grouper = new Grouper(elem, options);
  if (!grouper.uid) return null;
  Grouper._caches[grouper.uid] = grouper;
  return grouper
}

export default Grouper;