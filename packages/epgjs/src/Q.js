/**
 * Q.js dom选择器
 * 可以简单操作dom
 */
import Sizzle from "./sizzle";

function isObviousHtml(input) {
  return input[0] === "<" &&
    input[input.length - 1] === ">" &&
    input.length >= 3;
}
function isWindow(obj) {
  return obj != null && obj === obj.window;
}
function isArrayLike(obj) {
  var length = !!obj && obj.length,
    type = typeof obj;

  if (type === "function" || isWindow(obj)) {
    return false;
  }
  return type === "array" || length === 0 ||
    typeof length === "number" && length > 0 && (length - 1) in obj;
}


function mergeIgnore(classname, ignoreClass) {
  classname = classname.split(" ");
  ignoreClass = ignoreClass.split(" ");
  let ret = [];
  classname.forEach((cn) => {
    if(cn && ignoreClass.indexOf(cn) < 0) {
      ret.push(cn)
    }
  })
  return ret;
}

/**
 * 数组去重
 */
function queue(arr) {
  let res = [];
  for (let i = 0; i < arr.length; i++) {
    if (res.indexOf(arr[i]) < 0) {
      res.push(arr[i])
    }
  }
  return res;
}

const slice = Array.prototype.slice;

function Q(selector, context) {
  return new Q.fn.init(selector, context)
}
Q.fn = Q.prototype = {
  selector: '',
  constructor: Q,
  length: 0,
  find(selector) {
    let ret = [];
    for (let i = 0; i < this.length; i++) {
      Sizzle(selector, this[i], ret);
    }
    ret = this.pushStack(queue(ret))
    ret.selector = this.selector ? this.selector + " " + selector : selector;
    return ret;
  },
  pushStack: function (elems) {
    var ret = this.merge(this.constructor(), elems);
    ret.prevObject = this;
    ret.context = this.context;
    return ret;
  },
  get: function (num) {
    if (num === undefined) {
      return slice.call(this)
    }
    return num < 0 ? this[num + this.length] : this[num]
  },
  eq: function (i) {
    var len = this.length,
      j = +i + (i < 0 ? len : 0);
    return this.pushStack(j >= 0 && j < len ? [this[j]] : []);
  },
  toArray: function () {
    return slice.call(this)
  },
  makeArray: function (arr, results) {
    var ret = results || [];
    if (arr != null) {
      if (isArrayLike(Object(arr))) {
        Q.merge(ret,
          typeof arr === "string" ?
            [arr] : arr
        );
      } else {
        push.call(ret, arr);
      }
    }

    return ret;
  },
  inArray: function (elem, arr, i) {
    return arr == null ? -1 : [].indexOf.call(arr, elem, i);
  },
  merge: function (first, second) {
    var len = +second.length,
      j = 0,
      i = first.length;

    for (; j < len; j++) {
      first[i++] = second[j];
    }

    first.length = i;

    return first;
  },
  /**
   * 获取第一个元素的csspath
   */
  cssPath: function(ignoreClass) {
    if(!this[0]) return;
    var el = this[0];
    var path = [];
    while (el && el.tagName !== 'HTML' && el.tagName !== 'BODY') {
      var selector = '';
      if(el.id) {
        selector += "#" + ("" + el.id).trim();
        path.unshift(selector);
        break;
      } else {
        selector = el.tagName.toLowerCase();
        var classname = mergeIgnore(el.getAttribute('class') || "", (ignoreClass || "") + " _selected")
        if(classname && classname.length) {
          var cl = selector + "." + classname.join(".");
          var scl = cl + (path.length > 0 ? ' > ' + path.join(" > ") : '')
          if(this.constructor(scl).length === 1) {
            path.unshift(cl);
            break;
          } else {
            var sib = el,  nth = 1;
            while (sib.nodeType === Node.ELEMENT_NODE && (sib = sib.previousSibling) && nth++);
            selector += ":nth-child("+nth+")";
            path.unshift(selector);
          }
        } else {
          var sib = el,  nth = 1;
          while (sib.nodeType === Node.ELEMENT_NODE && (sib = sib.previousSibling) && nth++);
          selector += ":nth-child("+nth+")";
          path.unshift(selector);
        }
      }
      el = el.parentNode;
    }
    return path.join(" > ");
  }
}


/**
 * Q构造函数
 */
const rquickExpr = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
let rootQ;
let init = Q.fn.init = function (selector, context, root) {
  var match, elem;
  if (!selector) {
    return this;
  }
  root = root || rootQ;
  if (selector.nodeType) {
    this.context = this[0] = selector;
    this.length = 1;
    return this;
  } else {
    match = selector + "";
    if (isObviousHtml(match)) {
      match = [null, selector, null];
    } else if (typeof selector === "string") {
      match = rquickExpr.exec(selector);
    } else if(selector instanceof Q) {
      return selector;
    } else {
      return Q.makeArray(selector, this);
    }
    if (match && (match[1] || !context)) {
      if (match[1]) {
        //对html标签进行dom话处理
        // context = context instanceof Q ? context[0] : context;
        // jQuery.merge(this, Q.parseHTML(
        //   match[1],
        //   context && context.nodeType ? context.ownerDocument || context : document,
        //   true
        // ));

        // // HANDLE: $(html, props)
        // if (rsingleTag.test(match[1]) && jQuery.isPlainObject(context)) {
        //   for (match in context) {

        //     // Properties of context are called as methods if possible
        //     if (typeof this[match] === "function") {
        //       this[match](context[match]);

        //       // ...and otherwise set as attributes
        //     } else {
        //       this.attr(match, context[match]);
        //     }
        //   }
        // }
        return this;
      } else {
        elem = document.getElementById(match[2]);
        if (elem) {
          this.context = document
          this[0] = elem;
          this.selector = selector;
          this.length = 1;
        }
        return this;
      }
    } else if (!context || context.Q) {
      return (context || root).find(selector);
    } else {
      return this.constructor(context).find(selector);
    }
  }
}
init.prototype = Q.fn;
rootQ = Q(document);

export default Q;

