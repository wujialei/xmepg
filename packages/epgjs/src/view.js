
/**
 * 不依赖
 * 管理层的操作区域
 * 视图区域需要主轴滚动，各个组内可以使用次轴滚动，需要对主轴和次轴的滚动进行缓存便于后续计算位置
 * 
 */
//dom选择器
import Q from './Q'
import Creater from './creater'
import Grouper from './grouper'
import Scroller from './scroller'
import { isFunction, getPosition, checkNext, eventFuncPromise } from './tools'
const E_FOCUS = 'focus';
const E_BEFOREFOCUS = 'beforefocus';
const E_BLUR = 'blur';
const E_BEFOREBLUR = 'beforeblur';
const E_CLICK = 'click';
const E_BACK = 'back';
const E_CSS_PATH = 'csspath'
const E_SCROLL = 'scroll'
function View(scope, options = {}) {
  //事件标记前缀
  this.$prefix = options.prefix;
  //函数作用域
  this.$scope = scope || {};
  //当前所在焦点
  this.$pointer = null;
  //当前焦点的csspath
  this.$pointerCssPath = '';
  //前一个焦点
  this.$lastPointer = null;
  //设置根节点
  this.$root = Q(options.root).get(0);
  //需要被扫描的元素
  this.selector = options.selector || '.epg-item';
  //统一焦点样式
  this.focusClass = options.focusClass || 'focus';
  //统一组选中样式
  this.selectedClass = options.selectedClass || 'selected';
  //扫描画布中的所有元素
  this.items = [];
  //页面中存在的所有滚轴
  this.scrollers = {};
  //所有组
  this.groupers = {};
  //是否正在运动
  this.isMoving = false;
  //扫描区域开启默认扫描
  options.autoScan !== false && this.scan();
}

View.prototype = {
  constructor: View,
  /**
   * 用于创建画布中的基础元素
   * @param {*} elm 
   * @returns 
   */
  creater(elm) {
    return Creater.create(elm, {
      focusClass: this.focusClass
    })
  },
  /**
   * 创建组元素
   * @param {*} elm 
   * @returns 
   */
  grouper(elm) {
    return Grouper.create(elm, {
      selector: this.selector,
      selectedClass: this.selectedClass
    })
  },
  /**
   * 创建滚轴对象
   * @param {*} elm 
   * @returns 
   */
  scroller(elm) {
    return Scroller.create(elm);
  },
  /**
   * 扫描获取到所有items元素
   */
  _scanItems() {
    let items = [];
    //扫描全部元素
    Q(this.$root).find(this.selector).toArray().forEach(elm => {
      items.push(this.creater(elm).update());
    });
    //移除无法扫描到的节点
    let deleteItems = [];
    this.items.forEach((creater) => {
      //删除不存在最新扫描中的集合元素
      if (items.indexOf(creater) < 0) {
        deleteItems.push(creater)
      }
    })
    //手动清理需要移除的节点
    Creater.recycle(deleteItems);
    //将页面中的全部新节点替换
    this.items = items;
    //触发元素在画布中的绝对位置，全部基于offsetTop, offsetLeft 进行获取
    this.items.forEach((creater) => {
      creater.setPos(this.getPosition(creater, this.$root, creater.pos, true));
    })
  },
  /**
   * 扫描画布中所有滚轴元素
   */
  _scanScroll() {
    let scrollers = {};
    let scrollElms = Q(this.$root).find('[scroll-container]').toArray();
    //扫描滚轴初始化，滚轴初始化存在当前所在滚轴逻辑问题
    scrollElms.forEach(elm => {
      let scroller = this.scroller(elm);
      if (scroller.uid) {
        scrollers[scroller.uid] = scroller;
      }
    })
    //初始化滚轴中的所有元素
    Object.keys(scrollers).forEach(uid => {
      let scroller = scrollers[uid];
      scroller.update();
      //查找滚轴是否存在父滚轴
      let psid = scroller.getParentScrollerId();
      psid && scrollers[psid] && scroller.setParentScroller(scrollers[psid]);
    })
    //执行滚轴元素扫描
    Object.keys(scrollers).forEach(uid => {
      let scroller = scrollers[uid];
      let items = Array.from(Q(scroller.box).find(this.selector))
      items.forEach(el => {
        if (scroller.isChild(el)) {
          this.creater(el).setScroller(scroller);
        }
      });
    })
    //触发元素在画布中的绝对位置，全部基于offsetTop, offsetLeft 进行获取
    this.items.forEach((creater) => {
      if (creater.scroller) {
        creater.setScrollPos(this.getPosition(creater, creater.scroller.$el, creater.scrollPos, true));
      }
    })
    this.scrollers = scrollers;
  },
  //扫描页面中的所有组
  _scanGroup() {
    Q(this.$root).find('[group-container]').toArray().forEach(elm => {
      let grouper = this.grouper(elm);
      if (grouper.uid) {
        this.groupers[grouper.uid] = grouper;
      }
    })

  },
  /**
   * 
   * 执行载入扫描视图中的节点
   */
  scan() {
    console.time('scan-items')
    this._scanItems();
    console.timeEnd('scan-items')
    //扫描滚轴
    console.time('scan-scroll')
    this._scanScroll();
    console.timeEnd('scan-scroll')
    //扫描组
    console.time('scan-group')
    this._scanGroup();
    console.timeEnd('scan-group')
    //判断当前焦点,是否有效
    if (!this.$pointer || this.items.indexOf(this.$pointer) < 0) {
      //判断是否有默认焦点配置
      let defPointer = this._getDefPointer();
      this._setPointer(defPointer || this.items[0])
    }
  },

  /**
   * 获取默认焦点，从前原则，找到则不再查找
   * @returns 
   */
  _getDefPointer() {
    let event = 'default';
    let pointer = null;
    for (let key in this.items) {
      let item = this.items[key];
      let attrs = item.attributes;
      let def = attrs[this.$prefix + event];
      if (def === '' || def) {
        pointer = item;
        break;
      }
    }
    return pointer;
  },

  /**
   * 设置焦点元素
   * @param {Creater} creater 
   */
  _setPointer(creater) {
    if (creater && this.items.includes(creater)) {
      let nexter = creater;
      let pointer = this.$pointer;
      let lastPointer = this.$lastPointer;
      let grouper = nexter.grouper || null;
      if (grouper) {
        //设置元素选中
        grouper.selected(nexter, pointer);
      }

      //设置当前元素失去焦点
      this.$lastPointer = this.$pointer;
      this.$lastPointer && this.$lastPointer.removeFocusClass();
      //pointer - Blur - 当前焦点已失去焦点
      let pointerBlur = this._getCreaterEvent(nexter, E_BLUR);
      if (isFunction(pointerBlur)) {
        eventFuncPromise(
          this._getEventParams(E_BLUR, nexter, pointer, lastPointer)
        )(pointerBlur)
      }

      //下一个获得焦点
      this.$pointer = creater;
      this.$pointer && this.$pointer.addFocusClass();
      //creater - Focus - 下一个焦点已获得焦点
      let nexterFocus = this._getCreaterEvent(nexter, E_FOCUS);
      if (isFunction(nexterFocus)) {
        eventFuncPromise(
          this._getEventParams(E_FOCUS, null)
        )(nexterFocus)
      }
      //创建焦点的csspath
      this.$pointerCssPath = Q(this.$pointer.$el).cssPath(this.$pointer.focusClass || "");
      this.setCssPath(this.$pointerCssPath)
    }
  },

  /**
   * 获取事件参数
   * @param {string} eventName //事件名
   * @param {Creater || null} nexter //下一个元素
   * @param {Creater || null} pointer //当前元素
   * @param {Creater || null} lastPointer //上一个元素
   * @param 
   * @returns 
   */
  _getEventParams(eventName, nexter, pointer, lastPonter) {
    return {
      //下一个焦点元素
      nexter: nexter || null,
      //当前焦点元素
      pointer: pointer || this.$pointer,
      //上一个元素
      laster: lastPonter || this.$lastPointer || null,
      //当前对象
      viewer: this,
      //事件名
      event: eventName || null
    }
  },

  /**
   * 触发其他事件
   * @param {*} action 
   * @param {*} value 
   */
  otherEvent(action, value) {
    action = action.slice(0, 1).toUpperCase() + action.slice(1);
    let eventName = 'Epg' + action;
    if (typeof this.$scope[eventName] === 'function') {
      this.$scope[eventName].call(this.$scope, value, this);
    }
  },

  /**
   * 移动到指定方向上
   * @param {*} direction 
   */
  move(direction) {
    if (!direction) return null;
    //移动 获取下一个元素
    let nexter;
    try {
      nexter = this._getNextCreater(direction)
    } catch (e) {
      console.error('getNextCreater-Error', e)
    }
    if (nexter) {
      this._move(nexter, direction);
    } else {
      //无方向行为不做方向事件判断
      //当前焦点在方向上的约束
      let directionFunc = this._getCreaterEvent(this.$pointer, direction)
      if (isFunction(directionFunc)) {
        eventFuncPromise(
          this._getEventParams(direction, nexter)
        )(directionFunc);
      }
    }
  },

  /**
   * 移动到指定元素上
   * @param {Creater/HTMLElement/CssPath} nexter 
   * @param {Function} eventFunc 
   */
  moveTo(nexter, scrollTime = null) {
    //移动到元素上
    let elm;
    if(!(nexter instanceof Creater)) {
      elm = Q(nexter).get(0)
    }
    if(!elm) {
      throw new Error("moveTo-nexter is empty") 
    }
    nexter = this.creater(elm)
    let w = nexter.$el.offsetWidth;
    let h = nexter.$el.offsetHeight;
    if (w === 0 || h === 0) {
      throw new Error("moveTo-nexter size is 0")
    }
    //获取最后一个元素
    this._move(nexter, null, scrollTime)
  },

  _move(nexter, direction = null, scrollTime = null) {
    if (this.isMoving) return console.log('isMoving');
    if (nexter === this.$pointer) return console.log("do not move", nexter);
    /**
     * 优先判焦点偏移逻辑
     * 组内移动方案
     */
    //即将移动到组内，当前元素所在与下一个元素所在组不同
    let grouper;
    if (nexter.grouper) {
      grouper = nexter.grouper;
    }
    if (nexter.grouper && nexter.grouper !== this.$pointer.grouper) {
      /**
       * 
       * 进入的元素为组元素，则按照组行为移动
       * 当前元素在组内，则移动
       * 当前元素在组外，则进入组,自动移动到选中上次的目标或者第一个目标
       */
      nexter = grouper.getSelected();
    }
    //开启行为,以下逻辑中出现异常立即终端后续操作
    let q = Promise.resolve();
    q.then(() => {
      //do nothing
    })
    .then(() => {
      //触发焦点事件 //pointer - BeforeBlur - 当前焦点即将失去焦点
      let pointerBeforeBlur = this._getCreaterEvent(this.$pointer, E_BEFOREBLUR);
      if (isFunction(pointerBeforeBlur)) {
        return eventFuncPromise(
          this._getEventParams(E_BEFOREBLUR, nexter)
        )(pointerBeforeBlur)
      }
    })
    .then(() => {
      //无方向行为不做方向事件判断
      if (direction) {
        //当前焦点在方向上的约束
        let directionFunc = this._getCreaterEvent(this.$pointer, direction)
        if (isFunction(directionFunc)) {
          return eventFuncPromise(
            this._getEventParams(direction, nexter)
          )(directionFunc);
        }
      }
    })
    .then(() => {
      //nexter - Beforefocus - 下一个焦点即将获得焦点
      let nexterBeforefocus = this._getCreaterEvent(nexter, E_BEFOREFOCUS);
      if (isFunction(nexterBeforefocus)) {
        return eventFuncPromise(
          this._getEventParams(E_BEFOREFOCUS, null, nexter, this.$pointer)
        )(nexterBeforefocus)
      }
    })
    .then(() => {
      //进行焦点默认行为控制,控制焦点样式等焦点行为处理
      this._setPointer(nexter);
    })
    .then(() => {
      //如果元素在滚轴内则处理定位逻辑
      //当前焦点在方向上的约束
      let scrollFunc = this._getCreaterEvent(this.$pointer, E_SCROLL)
      if (isFunction(scrollFunc)) {
        return eventFuncPromise(
          this._getEventParams(E_SCROLL, nexter)
        )(scrollFunc);
      } else if (nexter.scroller) {
        return nexter.scroller.scrollTo(nexter.$el)
      }
    })
    .then(() => {
    })
    .catch((e) => {
      throw new Error("move 异常")
    })
  },

  /**
   * 查询移动行为的下一个元素
   */
  _getNextCreater(direction) {
    //转换为 up/down/left/right
    let pointer = this.$pointer;
    let nexter;
    // 额外逻辑判断，若元素时滚轴内元素，则在轴内同方向行为下查找是否存在，若不存在再做全量排查
    // if(pointer.scroller) {
    //   let items = Array.from($(pointer.scroller.$el).find(this.selector)).map(elc => this.creater(elc))
    //   //尝试在滚轴内查询，查询不到在全局查询
    //   nexter = this.findNext(direction, pointer, items, pointer.scroller.$el, true);
    // }
    if (!nexter) {
      //获取元素信息
      console.time("nexter")
      nexter = this.findNext(direction, pointer, this.items, this.$root);
      console.timeEnd("nexter")
    }
    return nexter;
  },

  /**
   * 查询下一个焦点元素
   */
  findNext(direction, pointer, items, root, inner = false) {
    const distance = 99999999;
    //附近元素
    let nextObj = {
      pointer: pointer,
      next: null, //下个元素
      distance: distance, //最短距离
      maxDistance: distance, //最近距离
    }
    //元素的位置信息
    let pi = this.getScrollerPosition(pointer)
    for (let i = 0; i < items.length; i++) {
      let item = items[i];
      if (pointer === item) continue;
      let tw = item.$el.offsetWidth;
      let th = item.$el.offsetHeight;
      if (!tw || !th) continue;
      let ni = this.getScrollerPosition(item);
      if (!ni.w || !ni.h) continue;
      nextObj = checkNext(direction, pi, ni, item, nextObj);
    }
    return nextObj.next;
  },

  /**
   * 获取元素基于滚轴在画布上的位置
   */
  getScrollerPosition(creater) {
    let pos;
    if (creater.scroller) {
      pos = creater.scroller.getPosition(creater, this.$root);
    } else {
      pos = creater.pos;
    }
    return pos;
  },

  /**
   * 获取元素在根元素上的位置，理论是该数据只匹配一次，无需多次扫描
   * @param {*} creater 
   * @param {*} root 
   * @param {*} cache 缓存数据
   * @param {*} force 
   * @returns 
   */
  getPosition(creater, root, cache, force = false) {
    let pos = null;
    if (force || !cache) {
      pos = getPosition(creater.$el, root);
    } else {
      pos = cache;
    }
    pos.r = pos.l + pos.w;
    pos.b = pos.t + pos.h;
    return pos;
  },

  /**
   * 检查元素上的事件
   * eventName 
   * left/right/up/down
   * focus/beforefocus //已获得焦点/获得焦点前
   * blur/beforeblur //已失去焦点/失去焦点前
   * click //点击事件
   */
  _getCreaterEvent(creater, eventName) {
    if (!creater) return null;
    let attrs = creater.attributes
    let event = attrs[this.$prefix + eventName];
    //当定义了事件，但未定义内容，代表禁止事件
    if (event === '') {
      return () => { }
    } else if (event && typeof this.$scope[event] === 'function') {
      return this.$scope[event].bind(this.$scope)
    }
    return null;
  },

  /**
   * 触发的点击按键
   */
  click() {
    let pointer = this.$pointer;
    if (pointer && pointer.$el) {
      let func = this._getCreaterEvent(pointer, E_CLICK);
      //当前焦点，前一个焦点
      func && func(this._getEventParams(E_CLICK, null));
    }
  },

  /**
   * 触发的返回按键
   */
  back() {
    const runEpgBack = () => {
      if (typeof this.$scope.EpgBack == 'function') {
        this.$scope.EpgBack.call(this.$scope, this._getEventParams(E_BACK, null))
      }
    }
    let pointer = this.$pointer;
    let func;
    if (pointer && pointer.$el) {
      func = this._getCreaterEvent(pointer, E_BACK);
    }
    if (isFunction(func)) {
      eventFuncPromise(
        this._getEventParams(E_BACK, null)
      )(func).then(() => {
        runEpgBack();
      })
    } else {
      runEpgBack();
    }
  },

  setCssPath(cssPath) {
    if (typeof this.$scope.EpgFocusPath == 'function') {
      this.$scope.EpgFocusPath.call(this.$scope, this._getEventParams(E_CSS_PATH, null), cssPath)
    }
  },
}

export default View;
