/**
 * 不依赖jQuery
 * 滚轴样式
 */
const cssContents = [
  //滚轴样式
  `
/*垂直方向*/
.epg-scroll {
  position: relative;
  overflow-x: hidden;
  overflow-y: visible;
  overflow-y: scroll;
}
.epg-scroll::-webkit-scrollbar {
  width: 0;
  height: 0;
  color: transparent;
  display: none;
}
/*垂直方向绝对定位*/
[scroll-absolute].epg-scroll .epg-scroll-doc {
  position: absolute;
  top: 0;
  left: 0;
}
/*水平方向*/
.epg-scroll.epg-scroll_horiz {
  overflow: auto;
  overflow-y: hidden;
  overflow-x: visible;
  overflow-x: scroll;
}
.epg-scroll.epg-scroll_horiz .epg-scroll-doc{
  white-space: nowrap;
  position: absolute;
  left: 0;
  top: 0;
}
  `,
];

/**
 * 主动执行css引导，用于写入公共css
 */
export function bootstrapCss() {
  let sty = document.createElement("style");
  sty.setAttribute('key', "bootstrapCss");
  sty.innerHTML = cssContents.join("\n");
  let head = document.getElementsByTagName('head')
  head[0] && head[0].appendChild(sty)
}