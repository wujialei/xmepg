/**
 * 不依赖jQuery
 * 中介服务，用于解决业务需要
 */
import Action from './action'
import { KC_OK, KC_BACK, KC_LEFT, KC_RIGHT, KC_UP, KC_DOWN } from './config'
import View from './view'
import Creater from './creater'
import Q from './Q'
function Service(options) {
  options = options || {};
  this.operator = null;
  //存储所有视图层
  this.views = [];
  //设置及前缀
  this.prefix = options.prefix || "@";
  /**
   * 初始化按钮按键基础配置
   */
  this._init(options);
}

Service.prototype = {
  constructor: Service,
  
  /**
   * 初始化操作员
   * @param {*} options 
   */
  _init(options) {
    //操作对象
    this.operator = new Action({
      keyNumbers: options.keyNumbers || {},
      keyActions: options.keyActions || {},
      //keyCode处理
      keyBoardEvent: (keyCode) => {
        if (typeof options.keyBoardEvent == 'function') {
          return options.keyBoardEvent(keyCode)
        }
        return;
      },
      observe: (keyAction, value) => {
        this.triggerAction(keyAction, value)
      },
      interval: options.interval
    });
  },

  /**
   * 注册一个视图
   * @param {*} scope 
   * @param {*} root 
   * @returns 
   */
  registerView(options, scope) {
    this.views.push(
      new View(scope, {
        //扫描事件前缀
        prefix: this.prefix,
        //根节点，不建议使用body
        root: options.root,
        //epg元素的css选择器
        selector: options.selector,
        //组选择器样式
        selectedClass: options.selectedClass,
        //是否默认扫描，异步数据时建议使用false, 手动扫描
        autoScan: options.autoScan,
      }, this)
    );
    return this;
  },

  /**
   * 手动扫描或者视图更新扫描
   */
  scan() {
    let view = this.getCurrentView()
    view && view.scan();
  },

  /**
   * 注册视图对象
   * @param {*} keyAction 
   */
  triggerAction(keyAction, value) {
    switch (keyAction) {
      //确定事件
      case KC_OK:
        this.click();
        break;
      //返回事件
      case KC_BACK:
        this.back();
        break;
      case KC_LEFT: //左移动
      case KC_RIGHT: //右移动
      case KC_UP: //上移动
      case KC_DOWN: //下移动
        this.move(keyAction)
        break;
      default: 
        //其他事件
        this.otherEvent(keyAction, value);
        break;
    }
  },

  otherEvent(keyAction, value) {
    //简化方向事件名
    keyAction = keyAction.replace("KC_", '').toLowerCase();
    let view = this.getCurrentView();
    view && view.otherEvent(keyAction, value);
  },

  /**
   * 出发点击按键
   */
  click() {
    //在当前视图中操作
    let view = this.getCurrentView();
    view && view.click();
  },

  /**
   * 出发返回按键
   */
  back() {
    //在当前视图中操作
    let view = this.getCurrentView();
    if(view) {
      view.back();
    }
  },

  /**
   * 按照方向移动
   * @param {*} direction // up, down, right, left
   */
  move(direction) {
    //在当前视图中操作
    let view = this.getCurrentView();
    //简化方向事件名
    direction = direction.replace("KC_", '').toLowerCase();
    view && view.move(direction);
  },

  /**
   * 移动到指定元素
   * @param {Creater/HTML_ELEMENT/cssPath} target 移动到指定元素
   */
  moveTo(target, animateTime = null) {
    let view = this.getCurrentView();
    view && view.moveTo(target, animateTime);
  },

  /**
   * 获取元素的cssPath
   * @param {HTMLElement/Creater}
   */
  getCssPath(el) {
    if(el instanceof Creater) {
      return Q(el.$el).cssPath(el.focusClass) || ""
    } else {
      return Q(el).cssPath(Q(el).get(0).getAttribute('focusclass')) || ""
    }
  },

  //获取当前操作视图
  getCurrentView() {
    return this.views.slice(-1)[0];
  },

  //关闭当前可操作区域
  closeCurrentView() {
    if(this.views.length > 1) {
      //被移除的视图
      let previous = this.views.pop();
      let cur = this.getCurrentView();
      previous.$pointer.removeFocusClass();
      cur.$pointer.addFocusClass();
    }
    return this;
  }
}

export default Service;
