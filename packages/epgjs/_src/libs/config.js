/**
 * epg业务配置按键配置
 * KC_前缀,
 * 增加配置可支持自定义事件
 */

//OK或回车
export const KC_OK = 'KC_OK'
//返回键
export const KC_BACK = 'KC_BACK'
//方向键左
export const KC_LEFT = 'KC_LEFT'
//方向键右
export const KC_RIGHT = 'KC_RIGHT'
//方向键上
export const KC_UP = 'KC_UP'
//方向键下
export const KC_DOWN = 'KC_DOWN'
//数字事件
export const KC_NUMBER = 'KC_NUMBER'
export const keyActions = {
    KC_0: [48], //数字按键 0 
    KC_1: [49], //数字按键 1 
    KC_2: [50], //数字按键 2 
    KC_3: [51], //数字按键 3 
    KC_4: [52], //数字按键 4 
    KC_5: [53], //数字按键 5 
    KC_6: [54], //数字按键 6 
    KC_7: [55], //数字按键 7 
    KC_8: [56], //数字按键 8 
    KC_9: [57], //数字按键 9 
    KC_OK: [13],  //按键 确定
    KC_BACK: [
        8,
        32, /*空格键*/
        45, /*兼容云平台*/
        340, /*ipannel 返回*/
        1249, /*兼容烽火盒子*/
    ], //按键 返回
    KC_LEFT: [
        37,
        3,  /*ipannel*/
    ], //按键 向左
    KC_RIGHT: [
        39,
        4,  /*ipannel*/
    ], //按键 向左
    KC_UP: [
        38,
        1,  /*ipannel*/
    ], //按键 向上
    KC_DOWN: [
        40,
        2, /*ipannel*/
    ], //按键 向下
    KC_PAGEUP: [33], //上一页
    KC_PAGEDOWN: [34, 190], //下一页
    KC_VOLUP: [259], //音量 +
    KC_VOLDOWN: [260], //音量减
    KC_MUTE: [263, 261], //静音键
}

export const keyNumbers = {
    KC_0: 0, //数字按键 0 
    KC_1: 1, //数字按键 1 
    KC_2: 2, //数字按键 2 
    KC_3: 3, //数字按键 3 
    KC_4: 4, //数字按键 4 
    KC_5: 5, //数字按键 5 
    KC_6: 6, //数字按键 6 
    KC_7: 7, //数字按键 7 
    KC_8: 8, //数字按键 8 
    KC_9: 9, //数字按键 9 
}
