/**
 * 时间计时器
 */
function Timer(name) {
  if (Timer._caches[name] instanceof Timer) {
    return Timer._caches[name]
  }
  Timer._caches[name] = this;
  this._interval = false;
}

Timer.prototype = {
  constructor: Timer,
  /**
  * 
  * @param {*} time 时长 单位秒, 默认为300毫秒
  * @returns 
  */
  interval(time = 300) {
    time = time > 0 ? time : 300;
    //不可运行
    if (this._interval) return false;
    this._interval = true;
    setTimeout(() => {
      this._interval = false;
    }, time)
    return true;
  }
}

Timer._caches = {};
export default Timer;