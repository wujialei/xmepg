import Service from './service'
import Creater from './creater'
/**
 * 普通游览器版本
 * @param {*} options 配置参数
 * @param {*} scope 作用域
 * @returns 
 */
export default function browser(options, scope, defScan) {
  console.log(444, options, scope)
  let service = new Service(options);
  //注册第一个根节点
  service.registerView(scope, {
    root: options.root, 
    selector: options.selector, 
    scroller: options.scroller
  }, defScan);//建议设置根节点
  console.log('service', service)
  return service;
}

export const creater = function(el) {
  if(el instanceof Creater) return el;
  if(el instanceof $) {
    el = el.get(0)
  }
  if(el instanceof HTMLElement) {
    return Creater.create(el)
  }
  return null;
}