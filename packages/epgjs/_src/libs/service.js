import Action from './action'
import { KC_OK, KC_BACK, KC_LEFT, KC_RIGHT, KC_UP, KC_DOWN } from './config'
import View from './view'
import Creater from './creater'
import {getCssPath} from './tools'
/**
 * 中介服务，用于解决业务需要
 */
function Service(options) {
  options = options || {};
  this.operator = null;
  this.views = [];
  /**
   * 初始化
   */
  this.init(options);
}

Service.prototype = {
  constructor: Service,
  
  init(options) {
    //初始化操作事件绑定
    this.initOperator(options);
  },

  /**
   * 初始化操作员
   * @param {*} options 
   */
  initOperator(options) {
    this.operator = new Action({
      keyNumbers: options.keyNumbers || {},
      keyActions: options.keyActions || {},
      //keyCode处理
      keyBoardEvent: (keyCode) => {
        if (typeof options.keyBoardEvent == 'function') {
          return options.keyBoardEvent(keyCode)
        }
        return;
      },
      observe: (keyAction, value) => {
        this.triggerAction(keyAction, value)
      },
      interval: options.interval
    });
  },

  /**
   * 注册一个视图
   * @param {*} scope 
   * @param {*} root 
   * @returns 
   */
  registerView(scope, options, defScan) {
    this.views.push(
      new View(scope, {
        root: options.root,
        selector: options.selector,
        scroller: options.scroller
      }, this, defScan)
    );
    return this;
  },

  /**
   * 注册视图对象
   * @param {*} keyAction 
   */
  triggerAction(keyAction, value) {
    switch (keyAction) {
      //确定事件
      case KC_OK:
        this.click();
        break;
      //返回事件
      case KC_BACK:
        this.back();
        break;
      case KC_LEFT: //左移动
      case KC_RIGHT: //右移动
      case KC_UP: //上移动
      case KC_DOWN: //下移动
        this.move(keyAction)
        break;
      default: 
        //其他事件
        this.otherEvent(keyAction, value);
        break;
    }
  },

  otherEvent(keyAction, value) {
    //简化方向事件名
    keyAction = keyAction.replace("KC_", '').toLowerCase();
    let view = this.getCurrentView();
    view && view.otherEvent(keyAction, value);
  },

  /**
   * 出发点击按键
   */
  click() {
    //在当前视图中操作
    let view = this.getCurrentView();
    view && view.click();
  },

  /**
   * 出发返回按键
   */
  back() {
    //在当前视图中操作
    let view = this.getCurrentView();
    if(view) {
      view.back();
    }
  },

  /**
   * 按照方向移动
   * @param {*} target 
   */
  move(direction) {
    //在当前视图中操作
    let view = this.getCurrentView();
    //简化方向事件名
    direction = direction.replace("KC_", '').toLowerCase();
    view && view.move(direction);
  },

  /**
   * 移动到指定元素
   * @param {Creater/HTML_ELEMENT/cssPath} target 移动到指定元素
   */
  moveTo(target, animateTime = null) {
    let view = this.getCurrentView();
    view && view.moveTo(target, animateTime);
  },

  /**
   * 获取元素的cssPath
   * @param {HTMLElement/Creater}
   */
  getCssPath(el) {
    el = el instanceof Creater ? el.$el : el;
    return getCssPath(el)
  },

  //获取当前操作视图
  getCurrentView() {
    return this.views.slice(-1)[0];
  },

  //关闭当前可操作区域
  closeCurrentView() {
    if(this.views.length > 1) {
      this.views.pop();
    }
    return this;
  },

  /**
   * 手动移除控制节点
   * @param {HTMLElement/Creater} el 
   */
  removeItem(el) {
    let view = this.getCurrentView();
    view && view.removeItem(el); 
  },

  /**
   * 手动添加控制节点
   * @param {HTMLElement/Creater} el 
   */
  addItem(el) {
    let view = this.getCurrentView();
    view && view.addItems(el); 
  },

  /**
   * 视图更新/加载
   */
  load() {
    let view = this.getCurrentView()
    view && view.load();
  },

  groupLocation(el) {
    let view = this.getCurrentView()
    view && view.groupLocation(el);
  }
}

export default Service;
