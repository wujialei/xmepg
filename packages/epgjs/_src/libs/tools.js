/**
 * 基础工具类
 */

function is(o, type) {
  var isnan = { "NaN": 1, "Infinity": 1, "-Infinity": 1 }
  type = type.toLowerCase();
  if (type == "finite") {
    return !isnan["hasOwnProperty"](+o);
  }
  if (type == "array") {
    return o instanceof Array;
  }
  return (type == "null" && o === null) ||
    (type == typeof o && o !== null) ||
    (type == "object" && o === Object(o)) ||
    (type == "array" && Array.isArray && Array.isArray(o)) ||
    Object.prototype.toString.call(o).slice(8, -1).toLowerCase() == type;
}

export const isFunction = function (o) {
  return is(o, 'function')
};

/**
 * 创建一个promise实例, 取出resolve, reject
 * @return object {
 *   promise,
 *   resolve,
 *   reject
 * }
 */
 export const createPromise = function () {
  let _resolve = null
  let _reject = null
  const promise = new Promise((resolve, reject) => {
    _resolve = resolve
    _reject = reject
  })
  return {
    promise,
    resolve: _resolve,
    reject: _reject
  }
}

/**
 * 创建不可重复id
 * @returns 
 */
export const getUniqueId = () => {
  return (Math.random() + Date.now()).toString(36).replace(/\./, "")
}

/**
 * 检测对象是否含有对应的属性
 * @param {Object} obj 查询对象
 * @param {String} attr 查询属性
 * @returns Boolean 判断对象是否含有该属性
 */
export const hasOwn = (obj, attr) => Object.prototype.hasOwnProperty.call(obj, attr);


/**
 * 尝试元素转换为JSON对象
 * @param {*} value 
 * @returns 
 */
export function string2json(value) {
  try {
    return JSON.parse(value)
  } catch (e) {
    return value;
  }
}

/**
 * 尝试将数组转为JSON字符串
 * @param {*} obj 
 * @returns 
 */
export function json2string(obj) {
  if (typeof obj === 'object' && obj !== null) {
    return JSON.stringify(obj)
  }
  return obj;
}

/**
 * 获取元素dataset兼容处理
 * @param {DOM} el 元素节点
 * @param {String} name 属性名称
 */
export const getDataSet = (el, name) => {
  let dataset = {};
  let rep = new RegExp("-([a-z])", "gi");
  let nameStr =
    typeof name === "string"
      ? name.replace(rep, (all, letter) => letter.toUpperCase())
      : "";
  if (el.dataset) {
    if (nameStr) {
      return string2json(el.dataset[nameStr]);
    } else {
      Object.keys(el.dataset).forEach((k) => {
        dataset[k] = string2json(el.dataset[k])
      })
      return dataset;
    }
  } else {
    let attrs = el.attributes,
      reg = new RegExp("^data-(.+)"),
      repReg = new RegExp("-([a-z])", "gi");
    Array.from(attrs).forEach((value) => {
      if (!value.specified) return;
      let matchStr = value.name.match(reg);
      if (matchStr) {
        name = matchStr[1].replace(repReg, (all, letter) =>
          letter.toUpperCase()
        );
        dataset[name] = string2json(value.value);
      }
    });
    return nameStr ? dataset[nameStr] : dataset;
  }
};

/**
 * 设置dataset属性兼容方法
 * @param {DOM} el 元素节点
 * @param {String/Object} name 属性名称/需要设置的属性的对象集合
 * @param {String} value 属性值
 */
export const setDataSet = (el, name, value) => {
  if (typeof name === "string") {
    if (el.dataset) {
      el.dataset[name] = json2string(value);
    } else {
      let reg = new RegExp("([A-Z])", "g");
      let attr =
        "data-" +
        name.replace(reg, (match, letter) => "-" + letter.toLowerCase());
      el.attributes[attr] = json2string(value);
    }
  } else if (typeof name === "object" && name !== null) {
    for (let [k, v] of Object.entries(name)) {
      setDataSet(el, k, v);
    }
  }
};

/**
 * 获取元素属性
 * @param {*} elem 
 * @param {*} name  属性名
 * @returns 
 */
export const getAttr = function (elem, name) {
  let attrs = {};
  Array.from(elem.attributes).forEach(attr => {
    if (!attr.specified) return;
    let tname = attr.name;
    if (name && tname !== name) return;
    attrs[tname] = string2json(attr.value);
  })
  return name ? attrs[name] : attrs;
}

/**
 * 设置属性
 * @param {*} elem 
 * @param {Object/String} name  属性名
 * @param {*} value 属性值
 * @returns 
 */
export const setAttr = function (elem, name, value) {
  if(typeof name =='string') {
    elem.setAttribute(name, json2string(value))
  } else if(typeof name === 'object' && name !== null) {
    for (let [k, v] of Object.entries(name)) {
      elem.setAttribute(k, json2string(v))
    }
  }
}


/**
 * 是否是html元素
 */
export function isHTMLElement(el) {
  return el instanceof HTMLElement;
}

/**
 * 根据HTML_ELEMENT 获取cssPath
 * @param {*} el 
 * @returns 
 */
export function getCssPath(el) {
  el = $(el);
  let path = [];
  while (el && el[0].tagName !== 'HTML') {
    let selector = '';
    if (el.attr('id')) {
      //简化存在id的元素路径，直接使用id获取
      selector += '#' + el.attr('id');
      path = [selector];
      break;
    } else {
      selector = el[0].tagName.toLowerCase();
      if (el.attr('class') && el.attr('class') !== '' && el.attr('class').trim() !== '_selected' && selector !== 'input') {
        let classStr = el.attr('class');
        let cl = '.' + classStr.trim().split(/\s+/).join('.');
        if ($(selector + cl + '>' + (path.length > 0 ? path.join(" > ") : '')).length === 1) {
          path.unshift(selector + cl);
          break;
        }
        let suffixLen = el.nextAll();
        let nth = el.prevAll().length + 1;
        if (nth !== 1 || suffixLen !== 0)
          selector += ":nth-child(" + nth + ")";
      } else if (selector === 'input' && el.attr('name')) {
        if ($(selector + '[name=' + el.attr('name') + ']').length === 1) {
          path.unshift(selector + '[name=' + el.attr('name') + ']');
          break;
        }
      } else {
        let nth = el.prevAll().length + 1;
        if (nth !== 1)
          selector += ":nth-child(" + nth + ")";
      }
    }
    path.unshift(selector);
    el = el.parent();
  }
  return path.join(" > ");
}
