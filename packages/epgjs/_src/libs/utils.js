/**
 * 所有关于元素选择的工具函数均在Utils中
 */
/**
 * 计算两个左边间的距离
 * @param {*} cx 
 * @param {*} cy 
 * @param {*} nx 
 * @param {*} ny 
 * @returns 
 */
function distance(cx, cy, nx, ny) {
  return parseInt(Math.sqrt(Math.pow(cx - nx, 2) + Math.pow(cy - ny, 2)));
}

/**
 * 检查距离
 * @param {*} distances 
 * @param {*} el 
 * @param {*} nextObj {nextEl // 下个元素, minDistance, 最短距离}
 * 
 */
function checkDistances(distances, el, nextObj) {
  let min = Math.min.apply(null, distances);
  if (min < nextObj.distance) {
    nextObj.next = el;
    nextObj.distance = min;
  }
  return nextObj;
}

/**
 * 逐个检查可能的元素
 * 
 * @param {*} direction 方向
 * @param {*} prePosition 当前焦点元素定位信息
 * @param {*} elPosition  目标元素定位信息
 * @param {*} el 目标元素对象
 * @param {*} nextObj 对比信息
 * @returns 
 */
export function checkNext(direction, prePosition, elPosition, el, nextObj) {
  //当前焦点元素定位信息
  let pi = prePosition;
  //需要检查的元素定位信息
  let ni = elPosition;
  if (ni.w === 0 || ni.h === 0) return nextObj;
  if (direction === 'up' && pi.t >= ni.b) {
    /* 当前 必须在 目标下方 */
    //检查并替换
    nextObj = checkDistances([
      //当前左上 到 目标左下
      distance(pi.l, pi.t, ni.l, ni.b),
      //当前右上 到 目标右下
      distance(pi.r, pi.t, ni.r, ni.b),
    ], el, nextObj);

  } else if (direction === 'right' && pi.r <= ni.l) {
    /* 当前 必须在 目标左侧 */
    //检查并替换
    nextObj = checkDistances([
      //当前右上 到 目标左上
      distance(pi.r, pi.t, ni.l, ni.t),
      //当前右下 到 目标左下
      distance(pi.r, pi.b, ni.l, ni.b),
    ], el, nextObj);

  } else if (direction === 'down' && pi.b <= ni.t) {
    /* 当前 必须在 目标上方 */
    //检查并替换
    nextObj = checkDistances([
      //当前左下 到 目标左上
      distance(pi.l, pi.b, ni.l, ni.t),
      //当前右下 到 目标右上
      distance(pi.r, pi.b, ni.r, ni.t)
    ], el, nextObj);
  } else if (direction === 'left' && pi.l >= ni.r) {
    /* 当前 必须 目标右侧 */
    //检查并替换
    nextObj = checkDistances([
      //当前左上 到 目标右上
      distance(pi.l, pi.t, ni.r, ni.t),
      //当前左下 到 目标右下
      distance(pi.l, pi.b, ni.r, ni.b),
    ], el, nextObj);
  }
  return nextObj;
}



/**
 * 返回元素在可视区域内的元素宽高
 * @param {*} e 元素
 * @param {*} parent 滚动区域
 * @param {*} featureTop 即将移动的ScrollTop -1时不参与计算
 * @param {*} featrueLeft 即将移动的ScrollLeft -1时不参与计算
 */
export function getPostionInner(e, parent, featureTop = -1, featrueLeft = -1) {
  let pw = parent.offsetWidth;
  let ph = parent.offsetHeight;
  let { l, t, w, h } = getPositionDefault(e, parent);
  let top = $(parent).scrollTop() || 0;
  let left = $(parent).scrollLeft() || 0;
  //将预期参与计算
  if (featureTop >= 0) {
    top = featureTop;
  }
  //将预期参与计算
  if (featrueLeft >= 0) {
    left = featrueLeft;
  }
  let r = 0, b = 0, oldw = w, oldh = h;
  //重新定义高度
  l = l - left;
  r = l + w;
  t = t - top;
  b = t + h;
  //计算左右和上下在可视区域的逻辑
  // debugger;
  if (r <= 0 || l >= pw || t >= ph || b <= 0) {
    //底部超界
    w = 0, h = 0;
  } else {
    //左右计算
    if (l < 0 && r < pw) {
      l = 0;
      w = r;
    } else if (l > 0 && r > pw) {
      w = pw - l;
      r = pw;
    }
    //上下计算
    if (t < 0 && b < ph) {
      t = 0;
      h = b;
    } else if (t > 0 && b > ph) {
      h = ph - t;
      b = ph;
    }
  }
  //手动计算，将超出部分宽度或高度设置为0
  return {
    sl: left - oldw + w,
    st: top - oldh + h,
    l: l,
    t: t,
    r: l + w,
    b: t + h,
    w: w,
    h: h,
    ow: oldw,
    oh: oldh,
  };
}
window.getPostionInner = getPostionInner;


/**
 * 获取默认在页面中的定位
 * @param {*} e 
 * @param {*} parent 
 * @returns 
 */
export function getPositionDefault(e, parent = null) {
  let l = 0, t = 0, w = e.offsetWidth, h = e.offsetHeight;
  while (e != null && (parent == null || parent !== e)) {
    l += e.offsetLeft;
    t += e.offsetTop;
    e = e.offsetParent;
  }
  return {
    l: l,
    t: t,
    r: l + w,
    b: t + h,
    w: w,
    h: h
  };
}


/**
 * 获取当前焦点元素在页面中的绝对位置
 * @param {当前获取的焦点的元素} e 
 */
export function getPosition(e, parent = null, scroller = null) {
  let { l, t, w, h } = getPositionDefault(e, parent);
  let top = 0, left = 0;
  if (scroller) {
    top = $(scroller).scrollTop()
    left = $(scroller).scrollLeft();
  }
  l = l - left;
  t = t - top;
  //判断是否在内轴中
  let innerBox = $('[epg-group="'+e.epg.gid+'"]').get(0)
  let inner = {};
  if (innerBox) {
    inner = getPostionInner(e, innerBox);
  }
  w = inner.w === undefined ? w : inner.w;
  h = inner.h === undefined ? h : inner.h;
  l = l - (inner.sl === undefined ? 0 : inner.sl);
  t = t - (inner.st === undefined ? 0 : inner.st);
  return {
    l: l,
    t: t,
    r: l + w,
    b: t + h,
    w: w,
    h: h,
  };
}
window.getPosition = getPosition
/**
 * 获取元素位置和宽高信息
 * @param {*} target 
 * @returns 
 */
export function getClientRect(target) {
  let cr, width = 0, height = 0, centerx = 0, centery = 0;
  try {
    cr = target.getBoundingClientRect();
    centerx = cr.left + (cr.right - cr.left) / 2;
    centery = cr.top + (cr.bottom - cr.top) / 2;
    width = target.offsetWidth;
    height = target.offsetHeight;
  } catch (e) {
    console.log('no bounding', target)
    return false
  }
  return {
    l: centerx - width / 2,
    r: centerx + width / 2,
    t: centery - height / 2,
    b: centery + height / 2,
    w: width,
    h: height
  };
}
window.getClientRect = getClientRect;
