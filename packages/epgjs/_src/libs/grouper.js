import Creater from './creater'
import { getAttr, setAttr, getUniqueId, isHTMLElement } from './tools'
const EPG_GROUP = 'epg-group'
/**
 * 元素组，关于组的操作逻辑
 */
function Grouper(elem, options = {}) {
  if (!isHTMLElement(elem)) return {};
  this.attributes = getAttr(elem);
  let gid = this.attributes[EPG_GROUP];
  if (!gid) {
    gid = getUniqueId();
    setAttr(elem, EPG_GROUP, gid);
    this.attributes[EPG_GROUP] = gid;
  }
  this.$el = elem;
  this.selector = options.selector || '';
  this.selectedClass = options.selectedClass || 'epg-actived';
  this.gid = gid;
  //存放焦点元素
  this.items = [];
  //是否自动选中, 默认非自动选中
  this.isAuto = this.attributes['epg-group-auto'] !== undefined;
  //扫描组内元素
  this.load();
}

Grouper.prototype = {
  constructor: Grouper,
  load() {
    this.items = [];
    $(this.$el).find(this.selector).each((k, el) => {
      this.addItem(Creater.create(el).setGid(this.gid))
    })
  },

  /**
   * 获得组内的选中元素
   */
  getSelected() {
    let selected = [];
    for (let i = 0; i < this.items.length; i++) {
      let creater = this.items[i];
      if (creater.hasClass(this.selectedClass)) {
        selected.push(creater)
      }
    }
    return selected.length > 0 ? selected[0] : this.items[0]
  },

  selected(nexter, pointer) {
    if (!this.isAuto) return;
    //设置选中
    if (this.items.indexOf(pointer) >= 0) {
      //同轴元素
      pointer.removeClass(this.selectedClass)
    }
    //下一个元素添加
    if (this.items.indexOf(nexter) >= 0) {
      nexter.addClass(this.selectedClass)
    }
  },

  /**
   * 添加改区域下所有可操作元素
   */
  addItem(creater) {
    if (this.items.indexOf(creater) < 0) {
      this.items.push(creater);
    }
  },

  /**
   * 移除item元素
   * @param {*} creater 
   */
  removeItem(creater) {
    let index = this.items.indexOf(creater);
    if (index >= 0) {
      this.items.splice(index, 1)
    }
  },
}

Grouper._caches = {};
Grouper.create = function (elem, options) {
  let gid = getAttr(elem, 'epg-group');
  let grouper = Grouper._caches[gid];
  if (grouper && grouper instanceof Grouper) {
    return grouper;
  }
  grouper = new Grouper(elem, options);
  if (!grouper.gid) return null;
  Grouper._caches[grouper.gid] = grouper;
  return grouper
}

export default Grouper;