/**
 * 焦点元素
 */
import { getAttr, getDataSet, setDataSet, isHTMLElement, getUniqueId } from './tools'
/**
 * 创建可移动元素
 * @param {*} elem html_ELEMENT元素
 */
function Creater(elem, options = {}) {
  if(!isHTMLElement(elem)) return {};
  //存储dom节点
  this.$el = elem;
  //存储节点样式
  this.classname = '';
  //基础属性
  this.attributes = {};
  //data属性
  this.dataset = {}
  //当前元素的id
  let fid = getUniqueId();
  this.fid = fid;
  //所属组的id
  this.gid = null;
  this.$el.fid = this.setDataset('fid', fid);
  //首次更新
  this.updated();
  this.focusClass = options.focusClass || this.attributes.focusclass || 'focus';
  this.$el.epg = this;
}

Creater.prototype = {
  constructor: Creater,
  /**
   * 设置所在组的id
   */
  setGid(gid) {
    gid && (this.gid = gid);
    return this;
  },

  /**
   * 元素被更新时触发
   */
  updated() {
    this.attributes = getAttr(this.$el);
    this.classname = this.attributes.class || "";
    this.dataset = this._dataset();
    return this;
  },

  /**
   * 获取属性
   */
  _dataset() {
    return getDataSet(this.$el)
  },

  /**
   * 设置属性
   */
  setDataset(name, value) {
    setDataSet(this.$el, name, value)
    return this;
  },

  /**
   * 移除焦点样式
   */
  removeFocusClass() {
    this.removeClass(this.focusClass)
    return this;
  },

  /**
   * 追加焦点样式
   * @param {*} classname 
   */
  addFocusClass() {
    this.addClass(this.focusClass)
    return this;
  },

  /**
   * addClass
   */
  addClass(classname) {
    let cls = this.classname.split(' ');
    let inCls = classname.split(' ');
    inCls.forEach(v => {
      if (!cls.includes(v)) {
        cls.push(v)
      }
    })
    this.classname = cls.join(" ");
    //执行修改classs属性
    this.$el.setAttribute('class', this.classname);
    return this;
  },

  /**
   * removeClass
   */
  removeClass(classname) {
    let cls = this.classname.split(' ');
    let delCls = classname.split(' ');
    let inCls = [];
    cls.forEach(v => {
      if (!delCls.includes(v)) {
        inCls.push(v)
      }
    });
    //执行修改classs属性
    this.classname = inCls.join(" ");
    this.$el.setAttribute('class', this.classname);
    return this;
  },

  /**
   * hasClass
   * @param {*} classname 
   */
  hasClass(classname) {
    let cls = this.classname.split(' ');
    let checkCls = classname.split(' ');
    //判断样式是否存在
    let bool = true;
    checkCls.forEach(v => {
      if (!cls.includes(v)) {
        bool = false;
      }
    })
    return bool;
  },
}

Creater._caches = {};
Creater.create = function(elem, options) {
    if(!elem) return null;
    if(elem && elem instanceof Creater) {
      return elem;
    }
    let dataset = getDataSet(elem);
    let fid = dataset.fid;
    let creater = Creater._caches[fid];
    if(creater && creater instanceof Creater) {
      return creater;
    }
    creater = new Creater(elem, options);
    if(!creater.fid) return null;
    Creater._caches[creater.fid] = creater;
    return creater
  }


  /**
   * 用于回收资源
   * @param {*} creaters 
   */
Creater.recycle = function(creaters) {
    //情况用于缓存优化
    creaters.forEach(creater => {
      delete Creater._caches[creater.fid]
    })
  }



export default Creater;