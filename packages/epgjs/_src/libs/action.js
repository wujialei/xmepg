/**
 * EPG - 动作-操作,
 * 主要用于绑定EPG操作行为
 */
import { keyActions, keyNumbers, KC_NUMBER } from './config'
import Timer from './timer'
/**
 * 启动引导
 * @param {*} options 
 * {
 *    keyActions: <Object> , //自定义按钮事件 - 非必填
 *    keyNumbers: <Object> , //自定义数字按钮事件 - 非必填
 *    keyBoardEvent(keyCode)<function> //keyCode处理
 *    observe(keyAction)<function> //观察事件
 *    interval<number> //最短触发间隔
 * }
 */
function Action(options) {
  options = options || {};
  //设置按钮按键监听
  this.keyActions = Object.assign({}, keyActions, options.keyActions || {});
  //自定义数字按钮配置
  this.keyNumbers = Object.assign({}, keyNumbers, options.keyNumbers || {})
  //绑定自定义事件回调
  this.observe = typeof options.observe == 'function' ? options.observe : function () { }
  this.interval = options.interval > 0 ? options.interval : 250;
  //绑定按键行为
  this._keyBoardEventListener(options.keyBoardEvent);
  this.Timer = new Timer("Action");
}

Action.prototype = {
  constructor: Action,
  /**
   * 设置按钮按键行为
   */
  _keyBoardEventListener(callback) {
    const keyEvent = (event) => {
      if(!this.Timer.interval(this.interval)) return;
      //兼容低版本游览器
      event = event ? event : window.event;
      //获取按下的键盘按键Unicode值： var x = event.which; irfirefox2.0中不支持 window.event.keyCode,
      const keyCode = event.which ? event.which : event.keyCode;
      console.log("keyCode", keyCode)
      event.stopPropagation();
      event.preventDefault();
      let keyAction = this.getKeyAction(keyCode);
      if (keyAction) {
        //操作事件
        this.actionHandle(keyAction);
      }
      if (typeof callback === 'function') {
        return callback(keyCode, this);
      }
    }
    document.onkeydown = keyEvent;
    document.onkeypress = keyEvent;
    document.onsystemevent = keyEvent;
    document.onirkeypress = keyEvent;
  },

  /**
   * 获取按钮按键的事件名
   * @param {*} keyCode 
   */
  getKeyAction(keyCode) {
    //获取事件名
    let keyAction = null;
    const keyActions = this.keyActions;
    for (let actions in keyActions) {
      if (keyActions[actions].indexOf(keyCode) !== -1) {
        keyAction = actions;
        break;
      }
    }
    return keyAction;
  },

  /**
   * 按钮事件
   */
  actionHandle(keyAction) {
    let keyNumbers = this.keyNumbers;
    let num = keyNumbers[keyAction];
    if (num >= 0) {
      //触发数字事件
      return this.observe(KC_NUMBER, num)
    }
    return this.observe(keyAction)
  }
}

export default Action;