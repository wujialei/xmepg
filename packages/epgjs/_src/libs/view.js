
/**
 * 业务依赖jquery
 * 管理层的操作区域
 */
import Creater from './creater'
import Grouper from './grouper'
import { isFunction, createPromise, getAttr, getCssPath } from './tools'
import { getPosition, getPositionDefault, getPostionInner, checkNext } from './utils'

const E_LEFT = 'left';
const E_RIGHT = 'right';
const E_UP = 'up';
const E_DOWN = 'down';
const E_FOCUS = 'focus';
const E_BEFOREFOCUS = 'beforefocus';
const E_BLUR = 'blur';
const E_BEFOREBLUR = 'beforeblur';
const E_CLICK = 'click';
const E_BACK = 'back';
const E_CSS_PATH = 'csspath'

function eventFuncPromise(params) {
  return function (func) {
    return new Promise((reslove) => {
      let q = func(params, () => {
        reslove();
      })
      //支持promise行为
      if (q && q.then) {
        q.then((bool) => {
          bool && reslove();
        })
      }
      return q
    })
  }
}

function View(scope, options = {}, $epg, defScan = true){
  //函数作用域
  this.$scope = scope || {};
  //在$scope中添加$epg对象
  this.$scope.$epg = $epg;
  //当前所在焦点
  this.$pointer = null;
  //当前焦点的csspath
  this.$pointerCssPath = '';
  //前一个焦点
  this.$lastPointer = null;
  //设置根节点
  this.$parent = $(options.root).get(0) || $('body').get(0);
  //基础滚动轴
  this.$scroller = $(options.scroller || 'html,body').get(0);
  //需要被扫描的元素
  this.selector = options.selector || '.egp-item';
  //所有可操作元素
  this.items = [];
  //在scroll中的元素
  this.scrollItems = [];
  //所有组
  this.groups = {};
  this.isMoving = false;
  //扫描区域开启默认扫描
  console.log('def-scan', defScan)
  defScan && this.load();
}

View.prototype = {
  constructor: View,
  /**
   * 
   * 执行载入扫描视图中的节点
   */
  load() {
    let items = [];
    //扫描页面中现有节点
    //执行扫描
    $(this.$parent).find(this.selector).each((k, elm) => {
      items.push(Creater.create(elm))
    });
    //移除无法扫描到的节点
    let deleteItems = [];
    this.items.forEach((creater) => {
      //删除不存在最新扫描中的集合元素
      if(items.indexOf(creater) < 0) {
        deleteItems.push(creater)
      }
    })
    //手动执行节点移除
    Creater.recycle(deleteItems);
    //将新扫描的节点覆盖原节点集合
    this.items = items;
    //判断当前焦点,是否有效
    if(!this.$pointer || this.items.indexOf(this.$pointer) < 0) {
      //判断是否有默认焦点配置
      let defPointer = this._getDefPointer();
      this._setPointer(defPointer || this.items[0])
    }
    //初始化在scroll中的元素
    let scrollItems = [];
    $(this.$scroller).find(this.selector).each((k, elm) => {
      scrollItems.push(Creater.create(elm))
    });
    this.scrollItems = scrollItems;
    //扫描组
    let groups = $(this.$parent).find('[epg-group]');
    groups.each((k, group) => {
      let tmp = Grouper.create(group, {
        selector: this.selector
      });
      if (tmp.gid) {
        this.groups[tmp.gid] = tmp;
      }
    })
  },

  /**
   * 获取默认焦点，从前原则，找到则不再查找
   * @returns 
   */
  _getDefPointer() {
    let event = 'default';
    let pointer = null;
    for(let item of this.items) {
      let attrs = item.attributes;
      let def = attrs['@' + event];
      let pre = attrs['epg-' + event];
      if(def === '' || pre === '' || def || pre) {
        pointer = item;
      }
    }
    return pointer;
  },

  /**
   * 更具多条件创建可操作元素
   * @param {Creater/HTMLElement} creater 
   */
  _getCreaters(creater) {
    let tmps = [];
    if(! (creater instanceof Creater)) {
      //循环扫描到的节点
      $(creater).each((k, el) => {
        //创建之初多节点
        let tmp = Creater.create($(el).get(0))
        tmp && tmps.push(tmp)
      })
    } else {
      tmps.push(creater);
    }
    return tmps;
  },

  /**
   * 添加元素
   * @param {Creater/HTMLElement} creater 
   */
  addItem(creater) {
    let result = [];
    let tmps = this._getCreaters(creater)
    if(tmps.length) {
      tmps.forEach(tmp => {
        if(this.items.indexOf(tmp) < 0) {
          this.items.push(tmp);
        }
        result.push(tmp);
      })
    }
    return result;
  },

  /**
   * 移除item元素
   * @param {Creater/HTMLElement} creater 
   */
  removeItem(creater) {
    let result = [];
    let tmps = this._getCreaters(creater);
    if(tmps.length) {
      tmps.forEach(tmp => {
        let index = this.items.indexOf(tmp);
        if(index >= 0) {
          this.items.splice(index, 1)
          //手动执行节点移除
          Creater.recycle([tmp]);
          result.push(tmp)
        }
      })
    }
    return result;
  },

  /**
   * 设置焦点元素
   * @param {Creater} creater 
   */
  _setPointer(creater) {
    if (creater && this.items.includes(creater)) {
      let nexter = creater;
      let pointer = this.$pointer;
      let lastPointer = this.$lastPointer;
      let grouper = nexter.gid ? this.groups[nexter.gid] : null;
      if(grouper) {
        //设置元素选中
        grouper.selected(nexter, pointer);
      }

      //设置当前元素失去焦点
      this.$lastPointer = this.$pointer;
      this.$lastPointer && this.$lastPointer.removeFocusClass();
      //pointer - Blur - 当前焦点已失去焦点
      let pointerBlur = this._getCreaterEvent(nexter, E_BLUR);
      if (isFunction(pointerBlur)) {
        eventFuncPromise(
          this._getEventParams(E_BLUR, nexter, pointer, lastPointer)
        )(pointerBlur)
      }

      //下一个获得焦点
      this.$pointer = creater;
      this.$pointer && this.$pointer.addFocusClass();
      //creater - Focus - 下一个焦点已获得焦点
      let nexterFocus = this._getCreaterEvent(nexter, E_FOCUS);
      if (isFunction(nexterFocus)) {
        eventFuncPromise(
          this._getEventParams(E_FOCUS, null)
        )(nexterFocus)
      }
      //创建焦点的csspath
      this.$pointerCssPath = getCssPath(this.$pointer.$el);
      this.setCssPath(this.$pointerCssPath)
    }
  },

  /**
   * 获取事件参数
   * @param {string} eventName //事件名
   * @param {Creater || null} nexter //下一个元素
   * @param {Creater || null} pointer //当前元素
   * @param {Creater || null} lastPointer //上一个元素
   * @param 
   * @returns 
   */
  _getEventParams(eventName, nexter, pointer, lastPonter) {
    return {
      //下一个焦点元素
      nexter: nexter || null,
      //当前焦点元素
      pointer: pointer || this.$pointer,
      //上一个元素
      laster: lastPonter || this.$lastPointer || null,
      //当前对象
      viewer: this,
      //事件名
      event: eventName || null
    }
  },

  otherEvent(action, value) {
    action = action.slice(0,1).toUpperCase() + action.slice(1);
    let eventName = 'Epg' + action;
    if(typeof this.$scope[eventName] === 'function') {
      this.$scope[eventName].call(this.$scope, value, this);
    }
  },

  /**
   * 移动到指定方向上
   * @param {*} direction 
   */
   move(direction) {
    if (!direction) return null;
    //移动 获取下一个元素
    let nexter;
    try {
      nexter = this._getNextCreater(direction)
    } catch (e) {
      console.error('getNextCreater-Error', e)
    }
    if (nexter) {
      this._move(nexter, direction);
    } else {
      //无方向行为不做方向事件判断
      //当前焦点在方向上的约束
      let directionFunc = this._getCreaterEvent(this.$pointer, direction)
      if (isFunction(directionFunc)) {
        eventFuncPromise(
          this._getEventParams(direction, nexter)
        )(directionFunc);
      }
    }
  },

  async groupLocation(nexter) {
    //移动到元素上
    nexter = this.addItem(nexter);
    if (nexter.length < 1) return console.log("moveTo-nexter is empty");
    nexter = nexter.slice(-1)[0];
    /**
     * 优先判焦点偏移逻辑
     * 组内移动方案
     */
    //即将移动到组内，当前元素所在与下一个元素所在组不同
    let grouper;
    if(nexter.gid) {
      grouper = this.groups[nexter.gid];
    }
    if(grouper) {
      //在组个滚轴中移动
      await this.scrollLocation(nexter, grouper.$el, true);
      //设置元素选中
      grouper.selected(nexter, grouper.getSelected());
    }
  },

  /**
   * 移动到指定元素上
   * @param {Creater/HTMLElement/CssPath} nexter 
   * @param {Function} eventFunc 
   */
  moveTo(nexter, scrollTime = null) {
    //移动到元素上
    nexter = this.addItem(nexter);
    if (nexter.length < 1) return console.log("moveTo-nexter is empty");
    nexter = nexter.slice(-1)[0];
    let w = nexter.$el.offsetWidth;
    let h = nexter.$el.offsetHeight;
    if(w === 0 || h === 0) {
      return console.log("moveTo-nexter size is empty");
    }
    //获取最后一个元素
    this._move(nexter, null, scrollTime)
  },

  /**
   * 统一的移动操作, 标注各个阶段行为
   * 触发顺序 pointer.beforeblur -> nexter.beforefocus -> direction.event -> pointer.blur -> nexter.focus 
   * @param {*} nexter 
   * @param {*} direction 
   */
  async _move(nexter, direction = null, scrollTime = null) {
    if(this.isMoving) return console.log('isMoving');
    if(nexter === this.$pointer) return console.log("do not move");
    /**
     * 优先判焦点偏移逻辑
     * 组内移动方案
     */
    //即将移动到组内，当前元素所在与下一个元素所在组不同
    let grouper;
    if(nexter.gid) {
      grouper = this.groups[nexter.gid];
    }
    if (nexter.gid && nexter.gid != this.$pointer.gid) {
      /**
       * 
       * 进入的元素为组元素，则按照组行为移动
       * 当前元素在组内，则移动
       * 当前元素在组外，则进入组,自动移动到选中上次的目标或者第一个目标
       */
      nexter = grouper.getSelected();
    }
    if(grouper) {
      //在组个滚轴中移动
      this.isMoving = true;
      await this.scrollLocation(nexter, grouper.$el, true, scrollTime);
      this.isMoving = false;
    }
    //触发焦点事件
    //pointer - BeforeBlur - 当前焦点即将失去焦点
    let pointerBeforeBlur = this._getCreaterEvent(this.$pointer, E_BEFOREBLUR)
    if (isFunction(pointerBeforeBlur)) {
      await eventFuncPromise(
        this._getEventParams(E_BEFOREBLUR, nexter)
      )(pointerBeforeBlur)
    }

    //nexter - Beforefocus - 下一个焦点即将获得焦点
    let nexterBeforefocus = this._getCreaterEvent(nexter, E_BEFOREFOCUS);
    if (isFunction(nexterBeforefocus)) {
      await eventFuncPromise(
        this._getEventParams(E_BEFOREFOCUS, null, nexter, this.$pointer)
      )(nexterBeforefocus)
    }

    //无方向行为不做方向事件判断
    if(direction) {
      //当前焦点在方向上的约束
      let directionFunc = this._getCreaterEvent(this.$pointer, direction)
      if (isFunction(directionFunc)) {
        await eventFuncPromise(
          this._getEventParams(direction, nexter)
        )(directionFunc);
      }
    }


    //触发定位行为
    this.isMoving = true;
    await this.scrollLocation(nexter, this.$scroller, false, scrollTime);
    this.isMoving = false;
    // console.log("nexter", nexter)
    
    //进行焦点默认行为控制,控制焦点样式等焦点行为处理
    this._setPointer(nexter);
  },

  /**
   * 查询移动行为的下一个元素
   */
  _getNextCreater(direction) {
    //转换为 up/down/left/right
    let pointer = this.$pointer;
    //附近元素
    let nextObj = {
      pointer: pointer,
      next: null, //下个元素
      distance: 9999999999, //最短距离
    }
    //获取元素信息
    // let pi = getPosition(pointer.$el, null, this.scrollItems.includes(pointer) ? this.$scroller : null);
    let pi = getPosition(pointer.$el, null, null);
    let items = Array.from(this.items);
    for (let i = 0; i < items.length; i++) {
      let item = items[i];
      let tw = item.$el.offsetWidth;
      let th = item.$el.offsetHeight;
      if(!tw || !th) continue;
      // let ni = getPosition(item.$el, null,  this.scrollItems.includes(item) ? this.$scroller : null);
      let ni = getPosition(item.$el, null, null);
      nextObj = checkNext(direction, pi, ni, item, nextObj);
    }
    return nextObj.next;
  },

  /**
   * 检查元素上的事件
   * eventName 
   * left/right/up/down
   * focus/beforefocus //已获得焦点/获得焦点前
   * blur/beforeblur //已失去焦点/失去焦点前
   * click //点击事件
   */
  _getCreaterEvent(creater, eventName) {
    if(!creater) return null;
    let attrs = creater.attributes
    let defEvent = attrs['@' + eventName];
    let prefixEvent = attrs['epg-' + eventName];
    //当定义了事件，但未定义内容，代表禁止事件
    if (defEvent === '' || prefixEvent === '') {
      return () => { }
    }
    //优先识别defEvent @xxx的事件
    let event = defEvent || prefixEvent;
    if (event && typeof this.$scope[event] === 'function') {
      return this.$scope[event].bind(this.$scope)
    }
    return null;
  },

  /**
   * 触发的点击按键
   */
  click() {
    let pointer = this.$pointer;
    if (pointer && pointer.$el) {
      let func = this._getCreaterEvent(pointer, E_CLICK);
      //当前焦点，前一个焦点
      func && func(this._getEventParams(E_CLICK, null));
    }
  },

  /**
   * 触发的返回按键
   */
  async back() {
    let pointer = this.$pointer;
    if (pointer && pointer.$el) {
      let func = this._getCreaterEvent(pointer, E_BACK);
      if(isFunction(func)) {
        await eventFuncPromise(
          this._getEventParams(E_BACK, null)
        )(func);
      }
    }
    if(typeof this.$scope.EpgBack == 'function') {
      this.$scope.EpgBack.call(this.$scope, this._getEventParams(E_BACK, null))
    }
  },

  setCssPath(cssPath) {
    if(typeof this.$scope.EpgFocusPath == 'function') {
      this.$scope.EpgFocusPath.call(this.$scope, this._getEventParams(E_CSS_PATH, null), cssPath)
    }
  },

  /**
   * 元素滚动轴定位
   * @param {*} nexter 即将定位的元素
   */
  scrollLocation(nexter, scroller, inner = false, scrollTime = null) {
    let q = createPromise();
    //先按照主轴定位
    //无滚轴元素或定位元素时，触发定位行为, 元素不在滚轴内
    //目标元素
    let next = $(nexter.$el);
    //滚轴
    scroller = $(scroller);
    //判断滚轴方向，默认为垂直方向, 当存在属性 scroll-horiz 代表水平
    let isHoriz = scroller.attr("scroll-horiz") !== undefined;
    let align = scroller.attr("scroll-align");
    let gap = Number(scroller.attr("scroll-align-gap")) || 0; //scroll-align top/bottm偏移量
    let alimit = Number(scroller.attr("scroll-align-limit"));
    let alignLimit = 1;
    if(alimit > 0 && alimit <= 1)  alignLimit = alimit
    //动画持续时间
    let animateTime = Number(scroller.attr('scroll-animate-time')) || 500;
    if(scrollTime === 0) {
      animateTime = 0;
    }
    //滚轴文档
    let scrollerDoc = scroller.find('[scroll-document]').eq(0);
    if(scroller.length < 1 || next.length < 1 || scroller.find(next).length < 1) {
      //不触发定位行为
      return q.resolve(-1);
    }
    let swh = 0; //容器宽高
    let dwh = 0; //文档宽高
    let slt = 0; //滚轴滚动方向上的滚轴位置
    let nlt = 0; //元素滚动主轴方向初点位置
    let nrb = 0; //元素滚动主轴方向尾点位置
    let nwh = 0; //元素滚动主轴方向上的宽高距离
    //滚动参数
    let animateKey = '';
    let pos;
    if(inner) {
      pos = getPositionDefault(next.get(0), scroller.get(0))
    } else {
      pos = getPosition(next.get(0), scroller.get(0))
    }
    if(isHoriz) {
      //水平方向
      swh = scroller.width();
      if(scrollerDoc && scrollerDoc.length) {
        dwh = scrollerDoc.width();
      }
      slt = scroller.scrollLeft();
      nlt = pos.l
      nrb = pos.r;
      nwh = pos.w;
      animateKey = "scrollLeft";
    } else {
      //垂直方向
      swh = scroller.height();
      if(scrollerDoc && scrollerDoc.length) {
        dwh = scrollerDoc.height();
      }
      slt = scroller.scrollTop();
      nlt = pos.t;
      nrb = pos.b;
      nwh = pos.h;
      animateKey = "scrollTop";
    }
    let stop = 0;
    let featureStop = 0;
    if (align === 'top') { //主轴方向顶部
      featureStop = nlt - gap;
    } else if (align === 'bottom') { //主轴方向尾部
      featureStop = nrb - swh + gap;
    } else { //主轴方向居中
      featureStop = nlt + nwh / 2 - swh / 2;
    }
  
    //期望滚动不能超过滚轴长度
    if(dwh > 0) {
      let maxStop = dwh - swh >= 0 ? dwh - swh : 0;
      featureStop > maxStop && (featureStop = maxStop);
    }
  
    //判断元素是否完成展示 
    if (nlt > slt && nrb < slt + swh && alignLimit === 1) {
      if (nlt < swh && nrb < swh) {
        stop = 0;
      } else {
        stop = slt;
      }
    } else {
      if (nlt < swh * alignLimit && nrb < swh * alignLimit) {
        stop = 0;
      } else {
        stop = featureStop >= 0 ? Math.floor(featureStop) : 0;
      }
    }
    // console.log(111, stop, slt)
    //准备移动
    // cb && cb(0, stop, slt);
    if (stop != slt) {
      //避免出现由于窗口过短导致无法全部展示，估修改元素必须完整展示, todo 
      scroller.animate({ [animateKey]: stop + 'px' }, animateTime, () => {
        q.resolve(1)
      });
    } else {
      q.resolve(1)
    }
    return q.promise;
  }
}

export default View;
