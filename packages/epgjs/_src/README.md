# EPG使用文档

> 本EPG焦点组件主要主要用于兼容Android4.0以上系统，需要使用jQuery1.x
> 服务电信/联调/移动机顶盒
> 为了兼容低版本android 整个项目中不使用Class 语法 避免Object.defineProperty 兼容问题

为何要开发EPG焦点库？

1. 为了便捷开发，减少开发人员在定位元素上的代码编写
3. 支持业务多级弹窗的选择焦点要求。
4. 支持各元素的自定义事件机制(left/right/up/down/focus/beforefocus/blur/beforeblur/click)
5. 支持自定义按钮按键行为事件

### 主要模块结构

```
.
├── README.md
└── src
    ├── bg.js //背景模块
    ├── epg.less //基础样式
    ├── libs
    │   ├── action.js //事件绑定捕获模块
    │   ├── config.js //自定义事件配置文件，可以重新替换
    │   ├── creater.js //焦点创建器
    │   ├── error.js  //EPG错误对象
    │   ├── grouper.js //组操作选择器
    │   ├── index.js //引用入口文件
    │   ├── service.js //层级关系处理
    │   ├── timer.js //计时器对象
    │   ├── tools.js //工具
    │   ├── utils.js //功能（关于焦点选择业务逻辑)
    │   └── view.js //视图、图层对象
    ├── logs.js //日志
    ├── route.js //路由模块
    └── tools.js //基本工具
```

### 使用方式

```

```


### Change-logs

* v1.0.0 基础版 2021-8-20

