/**
 * 用于输出EPG日志对象，由于EPG中调试存在难度，顾增加该调试弹层,用于打印日志
 */
import $ from 'jquery'
import './epg.less'

const bgHtml = `
 <div class="epg-body_logs" id="epg-body_logs">
  <div class="epg-body_logs-context"></div>
 </div>`;

const shadeId = "#epg-body_logs";

class EpgLogs {

  constructor() {
  }

  /**
   * 初始化背景层
   */
  init() {
    let bg = $(shadeId);
    if (bg.length) {
    } else {
      bg = $(bgHtml);
    }
    $('body').append(bg);
  }

  log(...args) {
    if(this.isShow()) {
      try {
        if(args.length < 1) return;
        let elem = $(shadeId).find('.epg-body_logs-context').eq(0);
        let date = ("" + new Date()).replace(/^.*?(\d{2}:\d{2}:\d{2}).*?$/, "$1")
        elem.prepend("<span>"+date+" 日志："+JSON.stringify(args)+"</span>");
        console.log(date, args)
      } catch(e) {
        console.log("error-log", e)
      }
    }
    return this;
  }
  
  show() {
    this.init();
    $(shadeId).show();
    return this;
  }

  hide() {
    this.init();
    $(shadeId).hide()
    return this;
  }

  isShow() {
    if($(shadeId).length < 1) return false;
    let display = $(shadeId).css('display')
    return display !== 'none';
  }

}

export default new EpgLogs();