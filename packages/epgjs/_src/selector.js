/**
 * 不依赖jQuery
 * 简易的选择器用户dom选择操作
 */

var RegexSelector = /^([#\.])?([[a-zA-Z0-9\-\_]+)$/;
/**
 * 解析selector
 * @param {*} selector 
 */
function complieSelector(selector) {
  var f = selector.replace(/([#|\.])/g, function (v) { return " " + v }).trim().split(/\s+/)
  var id = null, tag = null, classNames = [], valid = true;
  for (var i = 0; i < f.length; i++) {
    var matchs = f[i].match(RegexSelector);
    if (!matchs) {
      valid = false;
      break;
    }
    switch (matchs[1]) {
      case "#":
        if (id) {
          valid = false;
          break;
        }
        id = matchs[2];
        break;
      case ".":
        if (classNames.indexOf(matchs[2]) < 0) {
          classNames.push(matchs[2])
        }
        break;
      default:
        if (tag) {
          valid = false;
          break;
        }
        tag = matchs[2]
        break;
    }
  }
  return valid ? { selector: selector, id: id, tag: tag ? ("" + tag).toLowerCase() : null, classNames: classNames } : null;
}

/**
 * 判断class是否都存在
 * @param {*} selector 
 */
function hasAllClassnames(el, classnames) {
  var elClass = el.className.trim().split(" ");
  var bool = true;
  for (var i = 0; i < classnames.length; i++) {
    if (elClass.indexOf(classnames[i]) < 0) {
      bool = false;
      break;
    }
  }
  return bool;
}

/**
 * 查找id
 * @param {*} id 
 * @returns 
 */
function getElementById(id) {
  var t = document.getElementById(id);
  return t ? [t] : [];
}

/**
 * 查找元素 classname
 * @param {*} classname 
 * @param {*} node 
 * @returns 
 */
function getElementsByClassName(classname, node) {
  var results = [];
  if (node.querySelectorAll) {
    results = node.querySelectorAll('.' + classname);
  } else if (node.getElementsByClassName) {
    //判断有这个方法，则使用原来的方法；
    results = node.getElementsByClassName(classname);
  } else {
    var elems = node.getElementsByTagName("*");
    for (var i = 0; i < elems.length; i++) {
      if (elems[i].classname.indexOf(classname) != -1) {
        results[results.length] = elems[i]
      }
    }
  }
  return Array.from(results);
}

/**
 * 查找tag
 * @param {*} tag 
 * @param {*} node 
 * @returns 
 */
function getElementsByTagName(tag, node) {
  return Array.from(node.getElementsByTagName(tag))
}

function find(search, parent) {
  parent = parent || document;
  var selectorArr = search.split(/\s+/);
  //取出第一个条件
  var selector = selectorArr.shift();
  var complie = complieSelector(selector)
  var complieId = complie.id;
  var complieTag = complie.tag;
  var complieCls = complie.classNames;
  var tmps;
  if (complie.id) {
    tmps = getElementById(complieId);
  } else if (complieCls.length) {
    tmps = getElementsByClassName(complieCls[0], parent);
  } else {
    tmps = getElementsByTagName(complieTag, parent);
  }
  //过滤出符合条件的元素
  var targets = [];
  for (var i = 0; i < tmps.length; i++) {
    //判断是否符合条件
    var tmp = tmps[i];
    var realClass = hasAllClassnames(tmp, complieCls);
    var realId = !complieId ? true : !!complieId && complieId == tmp.id;
    var realTag = !complieTag ? true : !!complieTag && complieTag == ("" + tmp.nodeName).toLowerCase();
    if (realClass && realId && realTag) {
      targets.push(tmp);
    }
  }
  //递归向下查找
  var result = [];
  if (selectorArr.length > 0) {
    for (var i = 0; i < targets.length; i++) {
      var tempResult = Array.prototype.slice.call(find(selectorArr.join(" "), targets[i]), 0);
      result = result.concat(tempResult);
    }
  } else {
    return targets;
  }
  return result;
}


/**
 * 
 * 类似querySelectorAll，用于获取游览器dom
 * @param {*} selector 选择器 #app.select tag.ddd, .app
 * @param {*} context 节点, 默认在全文当内查找
 */
export function SelectorAll(selector, context, lastOne) {
  //只获取第一个
  lastOne = !!lastOne;
  selector = selector.trim();
  if (selector.indexOf(',') >= 0) {
    selector = selector.split(/\s*,\s*/);
  } else {
    selector = [selector];
  }
  if(lastOne) {
    //条件倒序
    selector = selector.reverse();
  }
  var result = [];
  for (var i = 0; i < selector.length; i++) {
    result = result.concat(find(selector[i], context));
    if(lastOne && result.length) {
      break;
    }
  }
  return lastOne ? result[0] : result;
}

export function Selector(selector, context) {
  return SelectorAll(selector, context, true)
}